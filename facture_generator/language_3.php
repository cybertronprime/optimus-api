<?php
define("_VAT_NR","Ust IdNr.");
define("_INVOICE_NR","KOSTENRECHNUNG Nr.");
define("_CREDITNOTE_NR","Gutschrift Nr");
define("_SEE_DETAILED_TIMESHEET_AT_THE_BACK","Einzelpostenverrechnung auf der Rückseite");
define("_FEES","Honorar");
define("_EXPENSES","Kosten");
define("_SUBTOTAL","Zwischensumme (netto)");
define("_VAT","MwSt");
define("_VAT_NOT_APPLICABLE_1","Mehrwertsteuerfrei gemäss Artikel 259B CGI");
define("_VAT_NOT_APPLICABLE_2","Mehrwertsteuerfrei gemäss Artikel 293B CGI");
define("_DISBURSEMENTS","Auslagen");
define("_EXEMPTED_FROM_VAT","(Mehrwertsteuerfrei)");
define("_TOTAL_AMOUNT_DUE","Endsumme (brutto)");
define("_PAYABLE_UPON_RECEIPT","Sofort zahlbar nach Erhalt der Rechnung, ohne Abzug");
define("_PAYABLE_BY","per Scheck zu ADARIS oder überweisungsauftrag bei");
define("_LATE_PAYMENT","Verspätete Zahlung löst Zinsen in Höhe von dem Europäische Zentralbank Zinssatz + 10 punkten");

define("_DETAILED_TIMESHEET","Detaillierte Zustand");
define("_DILIGENCE_DATE","Datum");
define("_DILIGENCE_DESCRIPTION","Beschreibung");
define("_DILIGENCE_CATEGORY","Kategorie");
define("_DILIGENCE_TYPE","Typ");
define("_DILIGENCE_COEFFICIENT","Koef");
define("_DILIGENCE_RATE","Tarif");
define("_DILIGENCE_TOTAL","Gesamt");
?>