<?php
define("_VAT_NR","TVA");
define("_INVOICE_NR","Facture");
define("_CREDITNOTE_NR","Avoir");
define("_SEE_DETAILED_TIMESHEET_AT_THE_BACK","détail des diligences au verso");
define("_FEES","Honoraires");
define("_EXPENSES","Frais");
define("_SUBTOTAL","Total H.T.");
define("_VAT","TVA");
define("_VAT_NOT_APPLICABLE_1","NON APPLICABLE - ART. 259B CGI");
define("_VAT_NOT_APPLICABLE_2","NON APPLICABLE - ART. 293B CGI");
define("_DISBURSEMENTS","Debours");
define("_EXEMPTED_FROM_VAT","non soumis a TVA");
define("_TOTAL_AMOUNT_DUE","Total T.T.C.");
define("_PAYABLE_UPON_RECEIPT","Facture payable à réception");
define("_PAYABLE_BY","Par chèque ou par virement sur le compte ci-dessous");
define("_LATE_PAYMENT","Intérêts de retard au taux appliqué par la BCE à son opération de refinancement la plus récente + 10 pts.\nUne indemnité forfaitaire de 40€ est exigible de plein droit en cas de retard de paiement.\nLes frais de recouvrement exposés peuvent être mis en compte à titre complémentaire.");

define("_DETAILED_TIMESHEET","Etat détaillé des diligences");
define("_DILIGENCE_DATE","Date");
define("_DILIGENCE_DESCRIPTION","Description");
define("_DILIGENCE_CATEGORY","Categorie");
define("_DILIGENCE_TYPE","Type");
define("_DILIGENCE_COEFFICIENT","Coef");
define("_DILIGENCE_RATE","Tarif");
define("_DILIGENCE_TOTAL","Total");
?>