<?php
define("_VAT_NR","VAT Nr");
define("_INVOICE_NR","Invoice Nr");
define("_CREDITNOTE_NR","Credit Note Nr");
define("_SEE_DETAILED_TIMESHEET_AT_THE_BACK","see detailed timesheet at the back");
define("_FEES","Fees");
define("_EXPENSES","Expenses");
define("_SUBTOTAL","Subtotal");
define("_VAT","V.A.T.");
define("_VAT_NOT_APPLICABLE_1","NOT APPLICABLE - ART. 259B CGI");
define("_VAT_NOT_APPLICABLE_2","NOT APPLICABLE - ART. 293B CGI");
define("_DISBURSEMENTS","Disbursements");
define("_EXEMPTED_FROM_VAT","exempted from V.A.T.");
define("_TOTAL_AMOUNT_DUE","Total amount due");
define("_PAYABLE_UPON_RECEIPT","Payable upon receipt");
define("_PAYABLE_BY","By cheque or by wire transfer to the below account");
define("_LATE_PAYMENT","Late payment: interests rate applied by the European Central Bank to its latest refinancing operation + 10 points");

define("_DETAILED_TIMESHEET","Detailed Timesheet");
define("_DILIGENCE_DATE","Date");
define("_DILIGENCE_DESCRIPTION","Description");
define("_DILIGENCE_CATEGORY","Category");
define("_DILIGENCE_TYPE","Type");
define("_DILIGENCE_COEFFICIENT","Coef");
define("_DILIGENCE_RATE","Rate");
define("_DILIGENCE_TOTAL","Total");
?>