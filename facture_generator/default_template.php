<?php
$pdf->AddPage('P','A4');
$pdf->SetAutoPageBreak(1,10);
$pdf->SetDisplayMode('fullpage','single');
$pdf->SetLeftMargin(40); 

//COORDONNEES CLIENT
$pdf->SetFont('Arial','b',13);
$pdf->SetTextColor(0,0,0);

$pdf->SetXY(335,130);
$pdf->MultiCell(222,15,utf8_decode(_NOM),0,"L");

$pdf->SetFont('Arial','',13);
$pdf->SetX(335);
$pdf->MultiCell(222,15,utf8_decode(_ADRESSE),0,"L");
$pdf->SetX(335);
$pdf->MultiCell(222,15,utf8_decode(_PAYS),0,"L");

//TVA INTRACOM
$pdf->SetXY(335,230);
$pdf->MultiCell(222,15,_INTRACOM,0,"L");

//INFOS DOSSIER
$pdf->Image('folder_icon.gif',317,292,14);

$pdf->SetXY(335,290);
$pdf->MultiCell(222,15,_DOSSIER,0,"L");
$pdf->SetX(335);
$pdf->MultiCell(222,15,_NUMERO,0,"L");

//DATE
$pdf->SetXY(335,355);
$pdf->MultiCell(222,15,"Le " . _DATE_FACTURE,0,"L");

//NUMERO FACTURE
$pdf->SetY(410);
$pdf->SetFont('Arial','b',16);
$pdf->SetTextColor(0,0,0);
$pdf->MultiCell(516,16,mb_strtoupper((_TOTALTTC>0)?utf8_decode(_INVOICE_NR):utf8_decode(_CREDITNOTE_NR)) . " " . _NUMERO_FACTURE,0,'C');
 
//TABLEAU
$pdf->SetFont('Arial','',13);
$pdf->Cell(200,19,mb_strtoupper(utf8_decode(_FEES)) . " :","LT",0,"L");
$pdf->Cell(316,19,utf8_decode(_HONORAIRES) . " " . chr(128),"TR",1,"R");
$pdf->Cell(200,19,mb_strtoupper(utf8_decode(_EXPENSES)) . " :","L",0,"L");
$pdf->Cell(316,19,utf8_decode(_FRAIS) . " " . chr(128),"R",1,"R");
$pdf->Cell(200,19,mb_strtoupper(utf8_decode(_SUBTOTAL)) . " :","L",0,"L");
$pdf->Cell(316,19,utf8_decode(_TOTALHT) . " " . chr(128),"R",1,"R");
$pdf->Cell(200,19,mb_strtoupper(utf8_decode(_VAT)) . " (" . utf8_decode(_TAUXTVA) . ") :","L",0,"L");
$pdf->Cell(316,19,utf8_decode(_TVA) . " " . chr(128),"R",1,"R");

$pdf->Cell(200,19,mb_strtoupper(utf8_decode(_DISBURSEMENTS) . " " . utf8_decode(_EXEMPTED_FROM_VAT)) . " :","L",0,"L");
$pdf->Cell(316,19,utf8_decode(_DEBOURS) . " " . chr(128),"R",1,"R");
$pdf->Cell(200,19,"","L",0,"L");
$pdf->Cell(316,19,"","R",1,"R");
$pdf->SetFont('Arial','b',13);
$pdf->Cell(200,19,mb_strtoupper(utf8_decode(_TOTAL_AMOUNT_DUE)) . " :","LB",0,"L");
$pdf->Cell(316,19,utf8_decode(_TOTALTTC) . " " . chr(128),"RB",1,"R");
?>