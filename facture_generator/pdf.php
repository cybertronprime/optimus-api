<?php
$facture = $connection->query("SELECT * FROM `" . $input->db . "`.factures WHERE id = '" . $input->id . "'")->fetch(PDO::FETCH_ASSOC);
$intervention = $connection->query("SELECT * FROM `" . $facture['db'] . "`.dossiers_interventions WHERE id = '" . $facture['intervention'] . "'")->fetch(PDO::FETCH_ASSOC);
$dossier = $connection->query("SELECT * FROM `" . $facture['db'] . "`.dossiers WHERE id = '" . $facture['dossier'] . "'")->fetch(PDO::FETCH_ASSOC);
$client = $connection->query("SELECT * FROM `" . $facture['db'] . "`.contacts WHERE id = '" . $facture['client'] . "'")->fetch(PDO::FETCH_ASSOC);

if ($client)
{
	$pays = file_get_contents('https://api.optimus-avocats.fr/constants?data={"db":"pays"}');
	$pays = json_decode($pays,true);
	$pays_id = array_search($client['country'], array_column($pays['data'], 'id'));
	$pays = $pays['data'][$pays_id];
}

$tva_rate = file_get_contents('https://api.optimus-avocats.fr/constants?data={"db":"tva_rates"}');
$tva_rate = json_decode($tva_rate,true);
$tva_rate_id = array_search($facture['tva'], array_column($tva_rate['data'], 'id'));
$tva_rate = $tva_rate['data'][$tva_rate_id];

include_once 'api_optimus/facture_generator/language_'.$facture['language'].'.php';

if ($client['type']==30)
	define("_NOM",utf8_decode(strtoupper($client['company_name'])));
else
	define("_NOM",utf8_decode($client['firstname'] . " " . strtoupper($client['lastname'])));

if (preg_match('/cedex/i',$client['address']) > 0)
	define("_ADRESSE",utf8_decode(str_replace("<br>","\n",$client['address'])));
else
	define("_ADRESSE",utf8_decode(str_replace("<br>","\n",$client['address']) . "\n" . $client['zipcode'] . " " . strtoupper($client['city_name'])));

define("_PAYS",utf8_decode(strtoupper($pays['value'])));


if ($client['tva']!='')
	define("_INTRACOM",_VAT_NR . " : " . $client['tva']);
else
	define("_INTRACOM","");

define("_DOSSIER",$dossier['nom']);
define("_NUMERO",$dossier['numero']);

define("_DATE_FACTURE",date('d/m/Y',strtotime($facture['date'])));

define("_NUMERO_FACTURE",$facture['numero']);

define("_HONORAIRES",number_format($intervention['honoraires'],2,',',' '));

define("_FRAIS",number_format($intervention['frais'],2,',',' '));

define("_TOTALHT",number_format($intervention['honoraires']+$intervention['frais'],2,',',' '));
if ($facture['tva']==1 or $facture['tva']==2 or $facture['tva']==6)
	define("_TAUXTVA",$tva_rate['value'] . "%");
if ($facture['tva']==3)
	define("_TAUXTVA",_VAT_NOT_APPLICABLE_1);
if ($facture['tva']==4)
	define("_TAUXTVA",_VAT_NOT_APPLICABLE_1);
if ($facture['tva']==5)
	define("_TAUXTVA",_VAT_NOT_APPLICABLE_2);
define("_TVA",number_format(($intervention['honoraires']+$intervention['frais'])*$tva_rate['value']/100,2,',',' '));

define("_DEBOURS",number_format($intervention['debours'],2,',',' '));

define("_TOTALTTC",number_format($facture['total'],2,',',' '));

include('api_optimus/lib/fpdf/fpdf.php');
mb_internal_encoding("UTF-8");

$pdf=new FPDF('P','pt','A4');
$pdf->SetAuthor('OPTIMUS');
$pdf->SetCreator('OPTIMUS');
$pdf->SetTitle('FACTURE');

$pdf->AddPage('P','A4');
$pdf->SetAutoPageBreak(1,10);
$pdf->SetDisplayMode('fullpage','single');
$pdf->SetLeftMargin(40); 

//COORDONNEES CLIENT
$pdf->SetFont('Arial','b',13);
$pdf->SetTextColor(0,0,0);

$pdf->SetXY(335,130);
$pdf->MultiCell(222,15,utf8_decode(_NOM),0,"L");

$pdf->SetFont('Arial','',13);
$pdf->SetX(335);
$pdf->MultiCell(222,15,utf8_decode(_ADRESSE),0,"L");
$pdf->SetX(335);
$pdf->MultiCell(222,15,utf8_decode(_PAYS),0,"L");

//TVA INTRACOM
$pdf->SetXY(335,230);
$pdf->MultiCell(222,15,_INTRACOM,0,"L");

//INFOS DOSSIER
$pdf->Image('/srv/api/api_optimus/facture_generator/folder_icon.gif',317,277,14);

$pdf->SetXY(335,275);
$pdf->MultiCell(222,15,_DOSSIER,0,"L");
$pdf->SetX(335);
$pdf->MultiCell(222,15,_NUMERO,0,"L");

//DATE
$pdf->SetXY(335,335);
$pdf->MultiCell(222,15,"Le " . _DATE_FACTURE,0,"L");

//NUMERO FACTURE
$pdf->SetY(410);
$pdf->SetFont('Arial','b',16);
$pdf->SetTextColor(0,0,0);
$pdf->MultiCell(516,16,mb_strtoupper((_TOTALTTC>0)?utf8_decode(_INVOICE_NR):utf8_decode(_CREDITNOTE_NR)) . " " . _NUMERO_FACTURE,0,'C');
 
//TABLEAU
$pdf->SetFont('Arial','',13);
$pdf->Cell(200,19,mb_strtoupper(utf8_decode(_FEES)) . " :","LT",0,"L");
$pdf->Cell(316,19,utf8_decode(_HONORAIRES) . " " . chr(128),"TR",1,"R");
$pdf->Cell(200,19,mb_strtoupper(utf8_decode(_EXPENSES)) . " :","L",0,"L");
$pdf->Cell(316,19,utf8_decode(_FRAIS) . " " . chr(128),"R",1,"R");
$pdf->Cell(200,19,mb_strtoupper(utf8_decode(_SUBTOTAL)) . " :","L",0,"L");
$pdf->Cell(316,19,utf8_decode(_TOTALHT) . " " . chr(128),"R",1,"R");
$pdf->Cell(200,19,mb_strtoupper(utf8_decode(_VAT)) . " (" . utf8_decode(_TAUXTVA) . ") :","L",0,"L");
$pdf->Cell(316,19,utf8_decode(_TVA) . " " . chr(128),"R",1,"R");

$pdf->Cell(200,19,mb_strtoupper(utf8_decode(_DISBURSEMENTS) . " " . utf8_decode(_EXEMPTED_FROM_VAT)) . " :","L",0,"L");
$pdf->Cell(316,19,utf8_decode(_DEBOURS) . " " . chr(128),"R",1,"R");
$pdf->Cell(200,19,"","L",0,"L");
$pdf->Cell(316,19,"","R",1,"R");
$pdf->SetFont('Arial','b',13);
$pdf->Cell(200,19,mb_strtoupper(utf8_decode(_TOTAL_AMOUNT_DUE)) . " :","LB",0,"L");
$pdf->Cell(316,19,utf8_decode(_TOTALTTC) . " " . chr(128),"RB",1,"R");


if (@$input->body->detail==1)
	include('api_optimus/intervention_generator/pdf.php');
else if (file_exists("/srv/files/" . $input->db . "/==MODELES FACTURES==/" . $facture['template']))
{
	$random = substr(sha1(mt_rand()),0,16);
	$pdf->Output('/srv/api/tmp/' . $random . ".pdf","F");
	exec("LC_ALL=fr_FR.UTF-8 pdftk '/srv/files/" . $input->db . "/==MODELES FACTURES==/" . $facture['template'] . "' background '/srv/api/tmp/" . $random . ".pdf' output '/srv/api/tmp/" . $random . "-generated.pdf' 2>&1",$output);
	echo base64_encode(file_get_contents('/srv/api/tmp/' . $random . '-generated.pdf'));
	unlink('/srv/api/tmp/' . $random . '-generated.pdf');
	unlink('/srv/api/tmp/' .$random . '.pdf');
}
else
	echo base64_encode($pdf->Output($facture['numero'] . ".pdf","S"));
exit;
?>
