La documentation complète de l'api est disponible à l'adresse suivante : https://swagger.optimus-avocats.fr/

L'API  utilise l'authentification intégrée au serveur ALLSPARK.
Elle s'effectue via un token JWT stocké sur un cookie sécurisé HttpOnly

Par défaut l'api accepte exclusivement les requêtes émanant du domaine optimus-avocats.fr qui héberge la version du client optimus maintenue par les développeurs de l'association CYBERTRON.

Si vous souhaitez installer votre propre version du client, il est nécessaire de modifier le fichier index.php de l'api OPTIMUS pour qu'il accepte les requêtes provenant de votre propre domaine.
