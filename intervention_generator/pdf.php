<?php
if (!@$input->body->detail==1)
{
	$intervention = $connection->query("SELECT * FROM `" . $input->db . "`.dossiers_interventions WHERE id = '" . $input->subid . "'")->fetch(PDO::FETCH_ASSOC);
	$dossier = $connection->query("SELECT * FROM `" . $input->db . "`.dossiers WHERE id = '" . $intervention['dossier'] . "'")->fetch(PDO::FETCH_ASSOC);

	include_once 'api_optimus/facture_generator/language_1.php';
	
	include('api_optimus/lib/fpdf/fpdf.php');
	mb_internal_encoding("UTF-8");
	$pdf=new FPDF('P','pt','A4');
	$pdf->SetAuthor('OPTIMUS');
	$pdf->SetCreator('OPTIMUS');
	$pdf->SetTitle('INTERVENTION');
}

$pdf->AddPage("L","A4");
$pdf->SetLeftMargin(20);

//TITLE
$pdf->SetFont('Arial','B',20);
$pdf->SetTextColor(0,0,0);
if ($intervention['description'] != '')
	$pdf->Cell(760,50,utf8_decode(strtoupper($intervention['description'])),0,1,"C");
else
	$pdf->Cell(760,50,strtoupper('FICHE DE FACTURATION '.$intervention['id']),0,1,"C");

//THEAD
$pdf->SetFont('Arial','B',10);
$pdf->SetTextColor(255,255,255);
$pdf->SetFillColor( 84,121,176);
$pdf->Cell( 55,18,mb_strtoupper(_DILIGENCE_DATE),'LTB',0,'C',true);
$pdf->Cell(425,18,mb_strtoupper(_DILIGENCE_DESCRIPTION),'LTB',0,'C',true);
$pdf->Cell( 70,18,mb_strtoupper(_DILIGENCE_CATEGORY),'LTB',0,'C',true);
$pdf->Cell(100,18,mb_strtoupper(_DILIGENCE_TYPE),'LTB',0,'C',true);
$pdf->Cell( 50,18,mb_strtoupper(_DILIGENCE_COEFFICIENT),'LTB',0,'C',true);
$pdf->Cell( 50,18,mb_strtoupper(_DILIGENCE_RATE),'LTB',0,'C',true);
$pdf->Cell( 50,18,mb_strtoupper(_DILIGENCE_TOTAL),'LTBR',1,'C',true);

$categories = file_get_contents('https://api.optimus-avocats.fr/constants?data={"db":"diligences_categories"}');
$categories = json_decode($categories,true);

$types = file_get_contents('https://api.optimus-avocats.fr/constants?data={"db":"diligences_subcategories"}');
$types = json_decode($types,true);

//TBODY
if (!@$input->body->detail==1)
	$diligences_query = $connection->query("SELECT * FROM `" .  $input->db . "`.dossiers_interventions_diligences WHERE intervention = '" .  $input->subid . "'");
else
	$diligences_query = $connection->query("SELECT * FROM `" . $facture['db'] . "`.dossiers_interventions_diligences WHERE intervention = '" . $facture['intervention'] . "'");
while ($diligence = $diligences_query->fetch(PDO::FETCH_ASSOC))
{
	if (($diligence['coefficient'] * $diligence['tarif'])==0)
		continue;
	
	$type_id = array_search($diligence['type'], array_column($types['data'], 'id'));
	$type = $types['data'][$type_id];
	
	$category_id = array_search($diligence['categorie'], array_column($categories['data'], 'id'));
	$category = $categories['data'][$category_id];
	
	$x++;
	if ($x%2!=0)
		$pdf->SetFillColor(255,255,255);
	else
		$pdf->SetFillColor(235,241,255);
		
	$pdf->SetFont('Arial','',10);
	$pdf->SetTextColor(0,0,0);
	
	$diligence['description'] = explode('<br>',$diligence['description']);
	if (sizeof($diligence['description'])>1)
		$height = sizeof($diligence['description']) * 15;
	else
		$height = 18;
	
	$pdf->Cell( 55,$height,date('d/m/Y',strtotime($diligence['date'])),'LTB',0,'C',true);
	
	if (sizeof($diligence['description'])==1)
		$pdf->Cell(425,18,utf8_decode($diligence['description'][0]),'LTB',0,'L',true);
	else
	{
		$pdf->MultiCell(425,15,utf8_decode(implode("\r\n",$diligence['description'])),'LTB','L',true);
		$pdf->SetY($pdf->GetY()-$height);
		$pdf->SetX(518);
	}
	$pdf->Cell( 70,$height,utf8_decode($category['value']),'LTB',0,'L',true);
	$pdf->Cell(100,$height,utf8_decode($type['value']),'LTB',0,'L',true);
	$pdf->Cell( 50,$height,$diligence['coefficient'],'LTB',0,'R',true);
	$pdf->Cell( 50,$height,$diligence['tarif'],'LTB',0,'R',true);
	$pdf->Cell( 50,$height,number_format($diligence['coefficient'] * $diligence['tarif'],2,".",""),'LTBR',1,'R',true);
}

//TFOOT
$pdf->SetY($pdf->GetY()+15);
$pdf->SetFont('Arial','b',10);
$pdf->Cell( 730,12,mb_strtoupper(_FEES) . ' :',0,0,'R');
$pdf->SetFont('Arial','',10);
$pdf->Cell( 72,12,number_format($intervention['honoraires'],2,'.',' ') . " " . chr(128),0,1,'R');

$pdf->SetFont('Arial','b',10);
$pdf->Cell( 730,12,mb_strtoupper(_EXPENSES) . ' :',0,0,'R');
$pdf->SetFont('Arial','',10);
$pdf->Cell( 72,12,number_format($intervention['frais'],2,'.',' ') . " " . chr(128),0,1,'R');

$pdf->SetFont('Arial','b',10);
$pdf->Cell( 730,12,mb_strtoupper(_DISBURSEMENTS) . ' :',0,0,'R');
$pdf->SetFont('Arial','',10);
$pdf->Cell( 72,12,number_format($intervention['debours'],2,'.',' ') . " " . chr(128),0,1,'R');

if (@$input->body->detail==1 AND file_exists("/srv/files/" . $input->db . "/==MODELES FACTURES==/" . $facture['template']))
{
	$random = substr(sha1(mt_rand()),0,16);
	$pdf->Output('/srv/api/tmp/' . $random . ".pdf","F");
	exec("LC_ALL=fr_FR.UTF-8 pdftk '/srv/api/tmp/" . $random . ".pdf' cat 1 output '/srv/api/tmp/" . $random . "-page1.pdf'");
	exec("LC_ALL=fr_FR.UTF-8 pdftk '/srv/api/tmp/" . $random . ".pdf' cat 2-end output '/srv/api/tmp/" . $random . "-page2.pdf'");
	exec("LC_ALL=fr_FR.UTF-8 pdftk '/srv/files/" . $input->db . "/==MODELES FACTURES==/" . $facture['template'] . "' background '/srv/api/tmp/" . $random . "-page1.pdf' output '/srv/api/tmp/" . $random . "-page1-generated.pdf'");
	exec("LC_ALL=fr_FR.UTF-8 pdftk '/srv/api/tmp/" . $random . "-page1-generated.pdf' '/srv/api/tmp/" . $random . "-page2.pdf' cat output '/srv/api/tmp/" . $random . "-generated.pdf'");
	echo base64_encode(file_get_contents('/srv/api/tmp/' . $random . '-generated.pdf'));
	unlink('/srv/api/tmp/' . $random . '.pdf');
	unlink('/srv/api/tmp/' . $random . '-generated.pdf');
	unlink('/srv/api/tmp/' . $random . '-page1.pdf');
	unlink('/srv/api/tmp/' . $random . '-page1-generated.pdf');
	unlink('/srv/api/tmp/' . $random . '-page2.pdf');
}
else
	echo base64_encode($pdf->Output("FICHE " . $input->subid . ".pdf","S"));
exit;
?>