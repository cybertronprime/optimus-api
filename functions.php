<?php
function get_authorizations($connection, $db, $user, $resource, $id)
{
	if ($user === $db)
		return array('read' => 1, 'write' => 1, 'create' => 1, 'delete' => 1);
	
	if ($resource == 'factures' OR $resource == 'recettes' OR $resource == 'operations')
	{
		$authorizations = $connection->prepare("SELECT member FROM `" . $db . "`.members WHERE member = :member");
		$authorizations->bindParam(':member', $user);
		$authorizations->execute();
		if ($authorizations->rowCount() > 0)
			return array('read' => 1, 'write' => 1, 'create' => 1, 'delete' => 1);
	}
	
	if (@$id)
		$authorizations = $connection->prepare("SELECT `read`, `write`,`create`,`delete` FROM `" . $db . "`.authorizations WHERE email = :email AND (resource = '" . $resource . "' OR resource = '" . $resource . "." . $id . "') ORDER BY length(resource) DESC");
	else
		$authorizations = $connection->prepare("SELECT `read`, `write`,`create`,`delete` FROM `" . $db . "`.authorizations WHERE email = :email AND resource = '" . $resource . "' ORDER BY length(resource) DESC");
	$authorizations->bindParam(':email', $user);
	$authorizations->execute();
	$authorizations = $authorizations->fetch(PDO::FETCH_ASSOC);
	return $authorizations;
}

function validate_fields($connection, $table, $body)
{
	$database_fields = $connection->query("SELECT DISTINCT COLUMN_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" . $table . "'");
	while ($database_field = $database_fields->fetch(PDO::FETCH_ASSOC))
		$fields[$database_field['COLUMN_NAME']] = $database_field['DATA_TYPE'];
	foreach((@$body->fields?@$body->fields:$body) as $field => $value)
		if (@!array_key_exists((@$value->name?@$value->name:$field), $fields))
			die(json_encode(array("code" => 400, "message" => "Le champ '" . (@$value->name?@$value->name:$field) . "' n'existe pas dans la table " . $table)));
	return $fields;
}

function bind_param($connection, $key, $value, $data_type)
{
	if ($data_type == 'bit' OR $data_type == 'tinyint' OR $data_type == 'smallint' OR $data_type == 'mediumint' OR $data_type == 'int' OR $data_type == 'bigint')
	{
		if ($value==='')
			$connection->bindValue(':'.$key, null, PDO::PARAM_NULL);
		else
			$connection->bindParam(':'.$key, $value, PDO::PARAM_INT);
	}
	else if (($data_type == 'date' OR $data_type == 'datetime') AND $value ==='')
		$connection->bindValue(':'.$key, null, PDO::PARAM_NULL);
	else
		$connection->bindParam(':'.$key, $value, PDO::PARAM_STR);
}