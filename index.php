<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 'On');
if ($path[2]=='server_status')
	include_once 'api_allspark/status.php';

if (!preg_match("/^https:\/\/[a-z0-9]+\.optimus-avocats\.fr?(:\d{1,5})?$/",@$_SERVER['HTTP_ORIGIN']))
{
	header("Access-Control-Allow-Origin: " . @$_SERVER['HTTP_ORIGIN']);
	header("Access-Control-Allow-Credentials: true");
	http_response_code(401);
	die(json_encode(array("code" => 401, "message" => "Les requêtes depuis " . @$_SERVER['HTTP_ORIGIN'] . " ne sont pas autorisées")));
}
header("Access-Control-Allow-Origin: " . (isset($_SERVER['HTTP_ORIGIN'])?$_SERVER['HTTP_ORIGIN']:$_SERVER['SERVER_NAME']));
header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, OPTIONS");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Content-Type, Accept, Authorization");
header("Access-Control-Max-Age: 1");
header("Content-Type: application/json");
if ($_SERVER['REQUEST_METHOD'] == "OPTIONS") die(http_response_code(200));

$input = (object) array();

if (@$_GET['data'] && @$_GET['data']!='{}' && ($_SERVER['REQUEST_METHOD']=='GET' OR $_SERVER['REQUEST_METHOD']=='DELETE'))
	$input->body = json_decode(urldecode($_GET['data']));
else if (file_get_contents("php://input") && file_get_contents("php://input")!='{}')
	$input->body = json_decode(file_get_contents("php://input"));

if (json_last_error() !== JSON_ERROR_NONE)
	die(json_encode(array("code" => 400, "message" => 'Les données fournies ne sont pas dans un format JSON valide')));

if ($path[2]=='login')
	include_once 'api_allspark/login.php';
else if ($path[2]=='logout')
	include_once 'api_allspark/logout.php';
else if ($path[2]=='logged')
	include_once 'api_allspark/logged.php';
else if ($path[2]=='ping')
	die(json_encode(array("code" => 200, "data" => array("date"=>trim(file('api_optimus/VERSION')[0]),"version"=>trim(file('api_optimus/VERSION')[1])), "message" => 'API OPTIMUS')));
else if ($path[2]=='update')
	$path[3]='update';
else if ($path[2]=='users')
	$path[3]='users';

if (urldecode(@$path[2]) AND preg_match("/^[a-z0-9-@.]+$/", urldecode(@$path[2])))
	$input->db = urldecode(@$path[2]);
else
{
	http_response_code(400);
	die(json_encode(array("code" => 400, "message" => "Base de données invalide")));
}

if (@$path[3] AND preg_match("/^[a-z0-9_]+$/", @$path[3]))
	$input->resource = $path[3];
else
{
	http_response_code(400);
	die(json_encode(array("code" => 400, "message" => "Resource invalide")));
}

if (@$path[4])
{
	if (preg_match("/^\d+$/", @$path[4]))
		$input->id = intval($path[4]);
	else
	{
		http_response_code(400);
		die(json_encode(array("code" => 400, "message" => "Identifiant invalide")));
	}
}

if (@$path[5])
{
	if (preg_match("/^[a-z0-9_]+$/", @$path[5]))
		$input->subresource = $path[5];
	else
	{
		http_response_code(400);
		die(json_encode(array("code" => 400, "message" => "Sous-resource invalide")));
	}
}

if (@$path[6])
{
	if (preg_match("/^\d+$/", @$path[6]))
		$input->subid = $path[6];
	else
	{
		http_response_code(400);
		die(json_encode(array("code" => 400, "message" => "Identifiant de sous-resource invalide")));
	}
}

if (@$path[7])
{
	if (preg_match("/^[a-z0-9_]+$/", @$path[7]))
		$input->subsubresource = $path[7];
	else
	{
		http_response_code(400);
		die(json_encode(array("code" => 400, "message" => "Sous-sous-resource invalide")));
	}
}

if (@$path[8])
{
	if (preg_match("/^\d+$/", @$path[8]))
		$input->subsubid = $path[8];
	else
	{
		http_response_code(400);
		die(json_encode(array("code" => 400, "message" => "Identifiant de sous-sous-resource invalide")));
	}
}

include_once 'config.php';
include_once 'api_optimus/config.php';
include_once 'connect.php';

include_once 'api_allspark/auth.php';
$input->user = $payload['user']->email;

if (@$input->subsubresource && file_exists('api_optimus/resources/' . $input->resource . '_' . $input->subresource . '_' . $input->subsubresource . '.php'))
	include_once 'api_optimus/resources/' . $input->resource . '_' . $input->subresource . '_' . $input->subsubresource .'.php';
else if (@$input->subresource && file_exists('api_optimus/resources/' . $input->resource . '_' . $input->subresource  . '.php'))
	include_once 'api_optimus/resources/' . $input->resource . '_' . $input->subresource . '.php';
else if (file_exists('api_optimus/resources/' . $input->resource . '.php'))
	include_once 'api_optimus/resources/' . $input->resource . '.php';
else
{
	http_response_code(404);
	die(json_encode(array("code" => 404, "message" => "Resource inconnue")));
}

if ($_SERVER['REQUEST_METHOD']=='GET')
	$result = read($db,$input);
if ($_SERVER['REQUEST_METHOD']=='POST')
	$result = create($db,$input);
if ($_SERVER['REQUEST_METHOD']=='PATCH')
	$result = modify($db,$input);
if ($_SERVER['REQUEST_METHOD']=='DELETE')
	$result = delete($db,$input);

http_response_code($result['code']);
echo json_encode($result);
?>
