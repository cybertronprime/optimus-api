<?php

function datagrid_request($connection, $input)
{
	datagrid_validation($connection,$input);
	$query = datagrid_query($input, @$dblink, $connection);
	$results = datagrid_fetch($connection, $input, $query);
	$results = datagrid_sort($results,$input);
	if (is_array($results) AND $input->body->page AND $input->body->results)
		$results = datagrid_limit($results, $input->body->page, $input->body->results);
	return $results;
}

function datagrid_validation($connection,$input)
{
	if (@$input->body->fields)
		$input->fields = validate_fields($connection, $input->table, @$input->body->fields);
	
	if (@$input->body->fields)
		foreach(@$input->body->fields as $field)
			if(!preg_match("/^[a-z0-9_@.]+$/", @$field->name))
				die(json_encode(array("code" => 400, "message" => "Le champ 'fields=>name' est absent invalide")));
			else if(@$field->dblink AND !preg_match("/^[a-z0-9_]+$/", $field->dblink))
				die(json_encode(array("code" => 400, "message" => "Le champ 'fields=>dblink' est invalide")));
			//else if($field->filter)
				//die(json_encode(array("code" => 400, "message" => "'filter' invalide");
			else if(@$field->sort_rank AND !preg_match("/^\d+$/",$field->sort_rank))
				die(json_encode(array("code" => 400, "message" => "Le champ 'fields=>sort_rank' est invalide")));
			else if(@$field->sort_order AND $field->sort_order != 'ASC' AND $field->sort_order != 'DESC')
				die(json_encode(array("code" => 400, "message" => "Le champ 'fields=>sort_order' est invalide")));
			else if(@$field->show AND !preg_match("/^\d+$/",$field->show))
				die(json_encode(array("code" => 400, "message" => "Le champ 'fields=>show' est invalide")));
			else if(@$field->order AND !preg_match("/^\d+$/",$field->order))
				die(json_encode(array("code" => 400, "message" => "Le champ 'fields=>order' est invalide")));
			else if(@$field->width AND !preg_match("/^\d+$/",$field->width))
				die(json_encode(array("code" => 400, "message" => "Le champ 'fields=>width' est invalide")));
		
	foreach($input->body->advanced_search as $field)
		if(!preg_match("/^[a-z0-9_]+$/",$field->field))
			die(json_encode(array("code" => 400, "message" => "'field' invalide")));
		//else if($field->operator)
			//die(json_encode(array("code" => 400, "message" => "'operator' invalide");
		//else if($field->value)
			//die(json_encode(array("code" => 400, "message" => "'value' invalide");
		else if(@$field->next AND ($field->next != 'AND' AND $field->next != 'OR'))
			die(json_encode(array("code" => 400, "message" => "'next' invalide")));
	
	//if ($input->body->global_search)
		//die(json_encode(array("code" => 400, "message" => "'global_search' invalide")));
	
	if ($input->body->results AND !preg_match("/^\d+$/",$input->body->results))
		die(json_encode(array("code" => 400, "message" => "'results' invalide")));
	
	if ($input->body->page AND !preg_match("/^\d+$/",$input->body->page))
		die(json_encode(array("code" => 400, "message" => "'page' invalide")));
}

function datagrid_query($input, $dblink, $connection)
{
	//START
	$query = "SELECT SQL_CALC_FOUND_ROWS ";

	//CHAMPS
	foreach ($input->body->fields as $key => $column)
	$query .= "`" . $column->name . '`,';
	$query = substr($query,0,-1);

	//BASE
	$query .= " FROM `" . $input->db . "`.`" .  $input->table . "`";
	$query .= " WHERE 1=1";


	//GLOBAL SEARCH
	if (@$input->body->global_search)
	{
		$query .= ' AND (';
		foreach ($input->body->fields as $key => $column)
			if (@$column->dblink == null)
				$query .= "`" . $column->name . "` LIKE " . data_format('%'.$input->body->global_search . '%', $input->fields[$column->name], $connection) . " OR ";
			else
			{
				unset($rowsearch);
				foreach ($input->dblink->{$column->dblink} as $key => $value)
					if (preg_match("/" . addslashes($input->body->global_search) . "/i", $value))
						@$rowsearch[] = (is_numeric($key))? $key : data_format($key, $input->fields[$column->name], $connection);
				if (is_array(@$rowsearch))
					$query .= "`" . $column->name . "` IN (" . implode($rowsearch,',') . ") OR ";
			}
		$query = substr($query,0,-4) . ')';
	}

	//COLUMN SEARCH
	if (@$input->body->fields)
		foreach ($input->body->fields as $key => $column)
			if (@$column->filter)
				if (@$column->dblink == null)
				{
					if ($input->fields[$column->name]=='text' OR $input->fields[$column->name]=='longtext' OR $input->fields[$column->name]=='varchar' OR $input->fields[$column->name]=='date')
						$query .= " AND `" .$column->name . "` LIKE " . data_format('%' . $column->filter . '%', $input->fields[$column->name], $connection);
					else
						$query .= " AND `" .$column->name . "` = " . data_format($column->filter, $input->fields[$column->name], $connection);
				}
				else
				{
					unset($rowsearch);
					foreach ($input->dblink->{$column->dblink} as $key => $value)
						if (preg_match("/" . addslashes($column->filter) . "/i", $value))
							@$rowsearch[] = (is_numeric($key))? $key : data_format($key, $input->fields[$column->name], $connection);
					if (is_array(@$rowsearch))
						$query .= " AND `" . $column->name . "` IN (" . implode($rowsearch,',') . ")";
					else
						$query .= ' AND 1=0';
				}

	//ADVANCED SEARCH
	if ($input->body->advanced_search)
	{
		$query .= ' AND (';
		foreach ($input->body->advanced_search as $key => $search)
			if (isset($search->value))
			{
				$search->dblink = $input->body->fields[$search->field]->dblink;
				
				if (is_numeric($search->field))
					$search->field = $input->body->fields[$search->field]->name;
				
				if (@!$search->next)
					$search->nect = 'AND';
				
				if (@$search->dblink == null)
				{
					if ($search->operator == "==")
						$query .= "`" . $search->field . "` = " . data_format($search->value, $input->fields[$search->field], $connection) . " " . ($search->next=='AND'?'AND':'OR') . " ";
					if ($search->operator == "<>" OR $search->operator == ">" OR $search->operator == ">=" OR $search->operator == "<" OR $search->operator == "<=")
						$query .=  "`" . $search->field . "` " . $search->operator . " " . data_format($search->value, $input->fields[$search->field], $connection) . " " . ($search->next=='AND'?'AND':'OR') . " ";
					else if ($search->operator == "LIKE%")
						$query .= "`" . $search->field . "` LIKE " . data_format($search->value . '%', $input->fields[$search->field], $connection) . " " . ($search->next=='AND'?'AND':'OR') . " ";
					else if ($search->operator == "%LIKE%") 
						$query .= "`" . $search->field . "` LIKE " . data_format('%' . $search->value . '%', $input->fields[$search->field], $connection) . " " . ($search->next=='AND'?'AND':'OR') . " ";
					else if ($search->operator == "%LIKE")
						$query .= "`" . $search->field . "` LIKE " . data_format('%' . $search->value, $input->fields[$search->field], $connection) . " " . ($search->next=='AND'?'AND':'OR') . " ";
					else if ($search->operator == "NOT LIKE%")
						$query .= "`" . $search->field . "` NOT LIKE " . data_format($search->value . '%', $input->fields[$search->field], $connection) . " " . ($search->next=='AND'?'AND':'OR') . " ";
					else if ($search->operator == "%NOT LIKE%")
						$query .= "`" . $search->field . "` NOT LIKE " . data_format('%' . $search->value . '%', $input->fields[$search->field], $connection) . " " . ($search->next=='AND'?'AND':'OR') . " ";
					else if ($search->operator == "%NOT LIKE")
						$query .= "`" . $search->field . "` NOT LIKE " . data_format('%' . $search->value, $input->fields[$search->field], $connection) . " " .($search->next=='AND'?'AND':'OR') . " ";
					else if ($search->operator == "IS NULL")
						$query .= "`" . $search->field . "` IS NULL " . ($search->next=='AND'?'AND':'OR') . " ";
					else if ($search->operator == "IS NOT NULL")
						$query .= "`" . $search->field . "` IS NOT NULL " . ($search->next=='AND'?'AND':'OR') . " ";
				}
				else
				{
					unset($rowsearch);
					foreach ($input->dblink->{$search->dblink} as $key => $value)
						if ($search->operator == "==" AND strtolower($value) == strtolower($search->value))
							@$rowsearch[] = (is_numeric($key))? $key : data_format($key, $input->fields[$column->name], $connection);
						else if ($search->operator == "<>" AND $value != $search->value)
							@$rowsearch[] = (is_numeric($key))? $key : data_format($key, $input->fields[$column->name], $connection);
						else if ($search->operator == ">" AND $value > $search->value)
							@$rowsearch[] = (is_numeric($key))? $key : data_format($key, $input->fields[$column->name], $connection);
						else if ($search->operator == ">=" AND $value >= $search->value)
							@$rowsearch[] = (is_numeric($key))? $key : data_format($key, $input->fields[$column->name], $connection);
						else if ($search->operator == "<" AND $value < $search->value)
							@$rowsearch[] = (is_numeric($key))? $key : data_format($key, $input->fields[$column->name], $connection);
						else if ($search->operator == "<=" AND $value <= $search->value)
							@$rowsearch[] = (is_numeric($key))? $key : data_format($key, $input->fields[$column->name], $connection);
						else if ($search->operator == "LIKE%" AND preg_match("/^" . $search->value . "/i", $value))
							@$rowsearch[] = (is_numeric($key))? $key : data_format($key, $input->fields[$column->name], $connection);
						else if ($search->operator == "%LIKE%" AND preg_match("/" . $search->value . "/i", $value))
							@$rowsearch[] = (is_numeric($key))? $key : data_format($key, $input->fields[$column->name], $connection);
						else if ($search->operator == "%LIKE" AND preg_match("/" . $search->value . "$/i", $value))
							@$rowsearch[] = (is_numeric($key))? $key : data_format($key, $input->fields[$column->name], $connection);
						else if ($search->operator == "NOT LIKE%" AND !preg_match("/^" . $search->value . "/i", $value))
							@$rowsearch[] = (is_numeric($key))? $key : data_format($key, $input->fields[$column->name], $connection);
						else if ($search->operator == "%NOT LIKE%" AND !preg_match("/" . $search->value . "/i", $value))
							@$rowsearch[] = (is_numeric($key))? $key : data_format($key, $input->fields[$column->name], $connection);
						else if ($search->operator == "%NOT LIKE" AND !preg_match("/" . $search->value . "$/i", $value))
							@$rowsearch[] = (is_numeric($key))? $key : data_format($key, $input->fields[$column->name], $connection);
						else if ($search->operator == "IS NULL")
							$rowsearch = [0];
						else if ($search->operator == "IS NOT NULL" AND $key!=0)
							@$rowsearch[] = (is_numeric($key))? $key : data_format($key, $input->fields[$column->name], $connection);

						if (is_array($rowsearch))
							$query .= "`" . $search->field . "` IN (" . implode($rowsearch,',') . ") " . ($search->next=='AND'?'AND':'OR') . " ";
						else
							$query .= '1=0 ' . ($search->next=='AND'?'AND':'OR') . " ";
				}
			}
			$query = substr($query,0,-4) . ')';
		}
		return $query;
}


function datagrid_fetch($connection, $input, $query)
{
	$fetched_results = $connection->prepare($query);
	if($fetched_results->execute())
	{
		while($fetched_result = $fetched_results->fetch($input->body->array_type=='numeric'?PDO::FETCH_NUM:PDO::FETCH_ASSOC))
		{
			foreach ($input->body->fields as $key => $column)
				if (@$column->dblink)
					if ($input->body->array_type=='numeric')
						$fetched_result[$key] = array($fetched_result[$key],@$input->dblink->{$column->dblink}[$fetched_result[$key]]);
					else
						$fetched_result[$column->name] = array("id" => $fetched_result[$column->name], "value" => @$input->dblink->{$column->dblink}[$fetched_result[$column->name]], );

			$results[] = $fetched_result;
		}
		if (@$results)
			return @$results;
		else
			return false;
	}
	else
		return $fetched_results->errorInfo()[2];
}

function datagrid_sort($results, $input)
{
	if ($results)
	{
		@$array_sort_query = 'array_multisort(';
		for ($i=0; $i < sizeof(array_keys((array)$input->body->fields)); $i++)
			foreach($input->body->fields as $key => $column)
				if (@$column->sort_rank === $i)
				{
					@$array_sort_query .= '$column_' . $column->sort_rank . ', SORT_' . $column->sort_order . ', ';
					if (@$column->dblink)
						foreach(array_column($results,$input->body->array_type=='numeric'?$key:$input->body->fields[$key]->name) as $key => $value)
							${"column_".$i}[$key] = $input->body->array_type=='numeric'?$value[1]:$value['value'];
					else
						${"column_".$i} = array_column($results,$input->body->array_type=='numeric'?$key:$input->body->fields[$key]->name);
				}
		@$array_sort_query .= '$results);';
		eval($array_sort_query);
	}
	return $results;
}


function datagrid_limit($data,$page,$results)
{
	return array_slice($data,($page-1)*$results,$results);
}


function data_format($value, $type, $connection)
{
	if ($type=='date')
	{
		if (substr($value,0,1)=='%')
			$modifier = 1;
		if (substr($value,2+@$modifier,1)=='/' AND substr($value,5+@$modifier,1)=='/')
			return $connection->quote((@$modifier?'%':'') . substr($value,6+@$modifier,4) . '-' . substr($value,3+@$modifier,2) . '-' . substr($value,0+@$modifier,2) . (substr($value,-1)=='%'?'%':''));
		else
			return $connection->quote($value);
	}
	else if ($type=='bit' OR $type=='tinyint' OR $type=='smallint' OR $type=='mediumint' OR $type=='int' OR $type=='bigint')
		return intval($value);
	else if ($type=='decimal')
		return floatval($value);
	else
		return $connection->quote($value);
}
