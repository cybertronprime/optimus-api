<?php
include_once 'api_optimus/functions.php';

function read($connection,$input)
{
	//$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, $input->id);
	//if ($authorizations['read'] == 0)
		//return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour accéder à ce dossier");
	if (isset($input->id))
		$structures = $connection->query("SELECT * FROM `" . $input->db . "`.structures WHERE id = " . $input->id . " ORDER BY id ASC")->fetchAll(PDO::FETCH_ASSOC);
	else
		$structures = $connection->query("SELECT * FROM `" . $input->db . "`.structures ORDER BY id ASC")->fetchAll(PDO::FETCH_ASSOC);
	return array("code" => 200, "data" => $structures);
}

function create($connection,$input)
{
	return array("code" => 501, "message" => 'Méthode non implémentée');
}

function modify($connection,$input)
{
	return array("code" => 501, "message" => 'Méthode non implémentée');
}

function delete($connection,$input)
{
	return array("code" => 501, "message" => 'Méthode non implémentée');
}
?>
