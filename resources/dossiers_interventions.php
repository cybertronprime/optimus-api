<?php
include_once 'api_optimus/functions.php';

function read($connection,$input)
{
	include_once 'api_optimus/datagrid.php';
	$input->table = 'dossiers_interventions';
	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, @$input->id);
	if ($authorizations['read'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour accéder à ce dossier");
	
	if (@$input->body->format == 'pdf' AND @$input->id)
	{
		include_once 'api_optimus/intervention_generator/pdf.php';
	}
	else if (@$input->id && !isset($input->subid))
	{
		if (@$input->body)
		{
			$input->fields = validate_fields($connection, $input->table, array_combine($input->body,$input->body));
			$interventions = $connection->prepare("SELECT " . implode(',', $input->body) . " FROM `" . $input->db . "`." . $input->table . " WHERE dossier = :id");
		}
		else
			$interventions = $connection->prepare("SELECT * FROM `" . $input->db . "`." . $input->table . " WHERE dossier = :id");
		$interventions->bindParam(':id', $input->id, PDO::PARAM_INT);
		
		if($interventions->execute())
		{
			$interventions = $interventions->fetchAll(PDO::FETCH_ASSOC);
			return array("code" => 200, "data" => $interventions, "authorizations" => $authorizations);
		}
		else
			return array("code" => 400, "message" => $interventions->errorInfo()[2]);
	}
	else if (@$input->subid)
	{
		if (@$input->body)
		{
			$input->fields = validate_fields($connection, $input->table, array_combine($input->body,$input->body));
			$intervention = $connection->prepare("SELECT " . implode(',', $input->body) . " FROM `" . $input->db . "`." . $input->table . " WHERE id = :id");
		}
		else
			$intervention = $connection->prepare("SELECT * FROM `" . $input->db . "`." . $input->table . " WHERE id = :id");
		$intervention->bindParam(':id', $input->subid, PDO::PARAM_INT);
		$intervention->execute();
		if ($intervention->rowCount() == 0)
			return array("code" => 404, "message" => "Cette fiche d'intervention n'existe pas");
		else
		{
			$intervention = $intervention->fetchAll(PDO::FETCH_ASSOC);
			return array("code" => 200, "data" => $intervention, "authorizations" => $authorizations);
		}
	}
	else if (@$input->body)
	{
		$dossiers = $connection->query("SELECT id, nom FROM `" . $input->db . "`.dossiers");
		while($dossier = $dossiers->fetch(PDO::FETCH_ASSOC))
			$input->dblink->dossiers[$dossier['id']] = $dossier['nom'];
		
		$totals = $connection->query("SELECT id, honoraires, frais, debours FROM `" . $input->db . "`.dossiers_interventions");
		while($total = $totals->fetch(PDO::FETCH_ASSOC))
			$input->dblink->totals[$total['id']] = ($total['honoraires'] + $total['frais'] + $total['debours']);
		
		$factures = $connection->query("SELECT id, db FROM `" . $input->db . "`.dossiers_interventions");
		while($facture = $factures->fetch(PDO::FETCH_ASSOC))
			$input->dblink->factures[$facture['id']] = $facture['db'];

		$results = datagrid_request($connection, $input);
		$total = $connection->query('SELECT FOUND_ROWS()')->fetchColumn();
		return array("code" => 200, "data" => $results, 'authorizations' => $authorizations, "total" => $total);
	}
	return array("code" => 400, "message" => "Il n'a été renseigné ni 'identifiant' ni 'champs' dans la requête");
}


function create($connection,$input)
{
	$input->table = 'dossiers_interventions';
	
	if (!isset($input->body->date_ouverture))
		$input->body->date_ouverture = date('Y-m-d');
	else if (!preg_match("/\d{4}\-\d{2}-\d{2}/", $input->body->date_ouverture))
		return array("code" => 400, "message" => "Date d'ouverture invalide");

	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, $input->id);
	if ($authorizations['write'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier à ce dossier");
	
	$input->fields = validate_fields($connection, $input->table, $input->body);
	
	$query = "INSERT INTO `" . $input->db . "`." . $input->table . " SET ";
	foreach($input->body as $key => $value)
		if ($key!='id')
			$query .= $key.'=:'.$key.',';
	$query = substr($query,0,-1);

	$intervention = $connection->prepare($query);
	foreach($input->body as $key => $value)
		if ($key!='id')
			bind_param($intervention, $key, $value, $input->fields[$key]);
	
	if($intervention->execute())
	{
		$new_id = $connection->lastInsertId();
		$new_intervention = $connection->query("SELECT * FROM `" . $input->db . "`." . $input->table . " WHERE id = " . $new_id)->fetch(PDO::FETCH_ASSOC);
		return array("code" => 201, "data" => $new_intervention, "authorizations" => $authorizations);
	}
	else
		return array("code" => 400, "message" => $intervention->errorInfo()[2]);
}


function modify($connection,$input)
{
	$input->table = 'dossiers_interventions';
	if (!isset($input->subid)) return array("code" => 400, "message" => "Aucun identifiant de fiche d'intervention n'a été renseigné");
	if (@!$input->body) return array("code" => 400, "message" => "Aucune donnée à modifier n'a été transmise");
	if (isset($input->body->date_ouverture) && !preg_match("/\d{4}\-\d{2}-\d{2}/", $input->body->date_ouverture))
		return array("code" => 400, "message" => "Date d'ouverture invalide");
	
	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, @$input->id);
	if ($authorizations['write'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour effectuer cette action");
	
	$input->fields = validate_fields($connection, $input->table, $input->body);
	
	$exists = $connection->query("SELECT id FROM `" . $input->db . "`.dossiers_interventions WHERE id = " . $input->subid);
	if ($exists->rowCount() == 0)
		return array("code" => 404, "message" => "Cette fiche d'intervention n'existe pas");
	
	$query = "UPDATE `" . $input->db . "`." . $input->table . " SET ";
	foreach($input->body as $key => $value)
		if ($key!='id')
			$query .= $key.'=:'.$key.',';
	$query = substr($query,0,-1);
	$query .= " WHERE id = '" . $input->subid . "'";

	$intervention = $connection->prepare($query);
	foreach($input->body as $key => $value)
		if ($key!='id')
			bind_param($intervention, $key, $value, $input->fields[$key]);
	
	if($intervention->execute())
	{
		$intervention = $connection->query("SELECT * FROM `" . $input->db . "`." . $input->table . " WHERE id = " . $input->subid)->fetch(PDO::FETCH_ASSOC);
		return array("code" => 200, "data" => $intervention, "authorizations" => $authorizations);
	}
	else
		return array("code" => 400, "message" => $intervention->errorInfo()[2]);
}


function delete($connection,$input)
{
	if (!isset($input->subid))
		return array("code" => 400, "message" => "Aucun identifiant de fiche d'intervention n'a été renseigné");
	
	$intervention_exists = $connection->query("SELECT * FROM `" . $input->db . "`.dossiers_interventions WHERE id = " . $input->subid, PDO::FETCH_OBJ);
	if ($intervention_exists->rowCount() == 0)
		return array("code" => 404, "message" => "Cette fiche d'intervention n'existe pas");
	$intervention_exists = $intervention_exists->fetchObject();
	
	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, $input->id);
	if ($authorizations['write'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier ce dossier");
	
	if (!$connection->query("DELETE FROM `" . $input->db . "`.dossiers_interventions WHERE id = " . $input->subid))
		return array("code" => 400, "message" => $connection->errorInfo()[2]);
	
	if (!$connection->query("DELETE FROM `" . $input->db . "`.dossiers_interventions_diligences WHERE intervention = " . $input->subid))
		return array("code" => 400, "message" => $connection->errorInfo()[2]);
	
	return array("code" => 200);
}
?>
