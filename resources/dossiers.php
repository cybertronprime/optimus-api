<?php
include_once 'api_optimus/functions.php';

function read($connection, $input)
{
	include_once 'api_optimus/datagrid.php';
	$input->table = 'dossiers';
	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, @$input->id);
	if ($authorizations['read'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour accéder à ce dossier");
	
	if (@$input->id)
	{
		if (@$input->body)
		{
			$input->fields = validate_fields($connection, $input->table, array_combine($input->body,$input->body));
			$dossier = $connection->prepare("SELECT " . implode(',', $input->body) . " FROM `" . $input->db . "`." . $input->table . " WHERE id = :id");
		}
		else
			$dossier = $connection->prepare("SELECT * FROM `" . $input->db . "`." . $input->table . " WHERE id = :id");
		$dossier->bindParam(':id', $input->id, PDO::PARAM_INT);
		$dossier->execute();
		if ($dossier->rowCount() == 0)
			return array("code" => 404, "message" => "Ce dossier n'existe pas");
		else
		{
			$dossier = $dossier->fetchAll(PDO::FETCH_ASSOC);
			return array("code" => 200, "data" => $dossier, "authorizations" => $authorizations);
		}
	}
	else if (@$input->body->fields[0]->name)
	{
		$domaines = file_get_contents('https://api.optimus-avocats.fr/constants/?data={"db":"dossiers_domaines"}');
		$domaines = json_decode($domaines, true);
		foreach ($domaines['data'] as $domaine)
			$input->dblink->domaines[$domaine['id']] = $domaine['value'];
		
		$sous_domaines = file_get_contents('https://api.optimus-avocats.fr/constants/?data={"db":"dossiers_sousdomaines"}');
		$sous_domaines = json_decode($sous_domaines, true);
		foreach ($sous_domaines['data'] as $sous_domaine)
			$input->dblink->sous_domaines[$sous_domaine['id']] = $sous_domaine['value'];
			
		$results = datagrid_request($connection, $input);
		$total = $connection->query('SELECT FOUND_ROWS()')->fetchColumn();
		return array("code" => 200, "data" => $results, 'authorizations' => $authorizations, "total" => $total);
	}
	else if (@$input->body)
	{
		if (isset($input->body->fields))
		{
			$input->fields = validate_fields($connection, $input->table, array_combine($input->body->fields,$input->body->fields));
			$query = "SELECT " . implode(',', $input->body->fields) . " FROM `" . $input->db . "`." . $input->table . ' WHERE ';
		}
		else
			$query = "SELECT * FROM `" . $input->db . "`." . $input->table . ' WHERE ';
		
		if (@$input->body->filters)
			foreach(@$input->body->filters as $filter)
				foreach($filter as $key => $value)
					$query .= $key.'=:'.$key.'  AND  ';
		$query = substr($query,0,-7);

		$dossiers = $connection->prepare($query);
		if (@$input->body->filters)
			foreach(@$input->body->filters as $filter)
				foreach($filter as $key => $value)
					bind_param($dossiers, $key, $value, @$input->fields[$key]);
		
		if($dossiers->execute())
		{
			$data = $dossiers->fetchAll(PDO::FETCH_ASSOC);
			$total = sizeof($data);
			if (is_array($data) AND $input->body->page AND $input->body->results)
				$data = array_slice($data,($input->body->page-1)*$input->body->results,$input->body->results);
			return array("code" => 200, "data" => $data, "authorizations" => $authorizations, "total" => $total);
		}
		else
			return array("code" => 400, "message" => $dossiers->errorInfo()[2]);
	}
	else
	{
		$dossiers =  $connection->query("SELECT * FROM `" . $input->db . "`." . $input->table);
		return array("code" => 200, "data" => $dossiers->fetchAll(PDO::FETCH_ASSOC), "authorizations" => $authorizations);
	}
	return array("code" => 400, "message" => "Il n'a été renseigné ni 'identifiant' ni 'champs' dans la requête");
}


function create($connection, $input)
{
	$input->table = 'dossiers';
	
	if (isset($input->body->nom) AND (@$input->body->nom=='' OR @$input->body->nom == '.' OR @$input->body->nom == '..' OR !preg_match('/^[a-zA-Z0-9 ._@\-àâäéèêëïîôöùûüÿç()\']+$/', @$input->body->nom))) return array("code" => 400, "message" => "Nom de dossier invalide");
	if (isset($input->body->nom)) @$input->body->nom = trim(@$input->body->nom);
	
	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, @$input->id);
	if ($authorizations['create'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour effectuer cette action");

	if (@!$input->body->numero)
	{
		$last_numero = $connection->query("SELECT numero FROM `" . $input->db . "`." . $input->table . " WHERE numero LIKE '" . date('y') . "/%' ORDER BY id DESC LIMIT 1")->fetchObject();
		@$input->body->numero = date('y') . '/' . str_pad(intval(substr($last_numero->numero,3))+1, 4, "0", STR_PAD_LEFT);
	}
	if (@!$input->body->nom)
		@$input->body->nom = 'DOSSIER ' . time();
	if (@!$input->body->date_ouverture)
		@$input->body->date_ouverture = date('Y-m-d');
	
	if (@$input->body)
		$input->fields = validate_fields($connection, $input->table, $input->body);
	
	$exists = $connection->query("SELECT nom FROM `" . $input->db . "`.dossiers WHERE nom = '" . $input->body->nom . "'");
	if ($exists->rowCount() > 0)
		return array("code" => 409, "message" => "Un dossier portant ce nom existe déjà dans la base");
	
	if (is_dir('/srv/files/' . $input->db . '/==DOSSIERS==/'.$input->body->nom))
		return array("code" => 409, "message" => "Un dossier portant ce nom existe déjà sur le cloud");
	
	$query = "INSERT INTO `" . $input->db . "`." . $input->table . " SET ";
	
	if (@$input->body)
	{
		foreach($input->body as $key => $value)
			$query .= $key.'=:'.$key.',';
		$query = substr($query,0,-1);

		$dossier = $connection->prepare($query);
		foreach($input->body as $key => $value)
			bind_param($dossier, $key, $value, $input->fields[$key]);
	}

	if($dossier->execute())
	{
		@mkdir('/srv/files/' . $input->db . '/==DOSSIERS==', 0750);
		@mkdir('/srv/files/' . $input->db . '/==DOSSIERS==/'.$input->body->nom, 0750);
		umask(0);
		@mkdir('/srv/mailboxes/' . $input->db . '/==DOSSIERS==/'.$input->body->nom, 0770);
		@chgrp('/srv/mailboxes/' . $input->db . '/==DOSSIERS==/'.$input->body->nom, 'mailboxes');
		@mkdir('/srv/mailboxes/' . $input->db . '/==DOSSIERS==/'.$input->body->nom.'/tmp', 0770);
		@chgrp('/srv/mailboxes/' . $input->db . '/==DOSSIERS==/'.$input->body->nom.'/tmp', 'mailboxes');
		@file_put_contents('/srv/mailboxes/' . $input->db . '/subscriptions',"==DOSSIERS==/" . $input->body->nom . "\n", FILE_APPEND);
		$new_id = $connection->lastInsertId();
		$new_dossier = $connection->query("SELECT * FROM `" . $input->db . "`." . $input->table . " WHERE id = " . $new_id)->fetch(PDO::FETCH_ASSOC);
		return array("code" => 201, "data" => $new_dossier, "authorizations" => $authorizations);
	}
	else
		return array("code" => 400, "message" => $dossier->errorInfo()[2]);
}


function modify($connection,$input)
{
	$input->table = 'dossiers';
	if (@!$input->body) return array("code" => 400, "message" => "Aucune donnée à modifier n'a été transmise");
	if (isset($input->body->nom) AND (@$input->body->nom=='' OR @$input->body->nom == '.' OR @$input->body->nom == '..' OR !preg_match('/^[a-zA-Z0-9 ._@\-àâäéèêëïîôöùûüÿç()\']+$/', @$input->body->nom))) return array("code" => 400, "message" => "Nom de dossier invalide");
	if (isset($input->body->nom)) @$input->body->nom = trim(@$input->body->nom);
	
	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, @$input->id);
	if ($authorizations['write'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour effectuer cette action");
	
	$input->fields = validate_fields($connection, $input->table, $input->body);
	
	$exists = $connection->query("SELECT id FROM `" . $input->db . "`.dossiers WHERE id = " . $input->id);
	if ($exists->rowCount() == 0)
		return array("code" => 404, "message" => "Ce dossier n'existe pas");
	
	if (@$input->body->nom)
		$old_name = $connection->query("SELECT nom FROM `" . $input->db . "`.dossiers WHERE id = '" . $input->id . "'")->fetchObject();
	
	if (@$old_name->nom != @$input->body->nom)
	{
		$same_name_exists = $connection->prepare("SELECT nom FROM `" . $input->db . "`.dossiers WHERE nom = :nom");
		$same_name_exists->bindParam(':nom',$input->body->nom);
		$same_name_exists->execute();
		if ($same_name_exists->rowCount() > 0)
			return array("code" => 403, "message" => "Un dossier portant ce nom existe déjà");
	}
	
	$query = "UPDATE `" . $input->db . "`." . $input->table . " SET ";
	foreach($input->body as $key => $value)
		if ($key!='id')
			$query .= $key.'=:'.$key.',';
	$query = substr($query,0,-1);
	$query .= " WHERE id = '" . $input->id . "'";

	$dossier = $connection->prepare($query);
	foreach($input->body as $key => $value)
		if ($key!='id')
			bind_param($dossier, $key, $value, $input->fields[$key]);
	
	if($dossier->execute())
	{
		if (@$old_name->nom != @$input->body->nom)
		{
			@rename('/srv/files/' . $input->db . '/==DOSSIERS==/' . $old_name->nom, '/srv/files/' . $input->db . '/==DOSSIERS==/' . $input->body->nom);
			@rename('/srv/mailboxes/' . $input->db . '/==DOSSIERS==/' . mb_convert_encoding($old_name->nom, "UTF7-IMAP","UTF-8"), '/srv/mailboxes/' . $input->db . '/==DOSSIERS==/' . mb_convert_encoding($input->body->nom, "UTF7-IMAP","UTF-8"));
			$subscriptions = file_get_contents('/srv/mailboxes/' . $input->db . '/subscriptions');
			$subscriptions = str_replace('==DOSSIERS==/' . mb_convert_encoding($old_name->nom, "UTF7-IMAP","UTF-8"), '==DOSSIERS==/' . mb_convert_encoding($input->body->nom, "UTF7-IMAP","UTF-8"), $subscriptions);
			@file_put_contents('/srv/mailboxes/' . $input->db . '/subscriptions', $subscriptions);
		}
		return array("code" => 200, "authorizations" => $authorizations);
	}
	else
		return array("code" => 400, "message" => $dossier->errorInfo()[2]);
}


function delete($connection,$input)
{
	$input->table = 'dossiers';
	
	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, @$input->id);
	if ($authorizations['delete'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour effectuer cette action");

	$exists = $connection->query("SELECT nom FROM `" . $input->db . "`." . $input->table . " WHERE id = '" . $input->id . "'");
	if ($exists->rowCount() == 0)
		return array("code" => 404, "message" => "Ce dossier n'existe pas");
	else
		$dossier = $exists->fetchObject();

	$interventions_exists = $connection->query("SELECT id FROM `" . $input->db . "`.dossiers_interventions WHERE dossier = '" . $input->id . "'")->rowCount();
	if ($interventions_exists > 0)
		return array("code" => 400, "message" => "Ce dossier ne peut pas être supprimé car il contient des fiches d'intervention");

	//$factures_exists = $connection->query("SELECT id FROM optimus_user_1.interventions WHERE dossier = '" . $input->id . "' AND db IS NOT NULL")->rowCount();
	//if ($factures_exists > 0)
		//return array("code" => 400, "message" => "Ce dossier ne peut pas être supprimé car des factures le concernant ont été émises");

	$dossier_delete = $connection->query("DELETE FROM `" . $input->db . "`." . $input->table . " WHERE id = '" . $input->id . "'");
	$intervenants_delete = $connection->query("DELETE FROM `" . $input->db . "`.dossiers_intervenants WHERE dossier = '" . $input->id . "'");

	system('rm -R "/srv/files/' . $input->db . '/==DOSSIERS==/' . $dossier->nom . '"');
	system('rm -R "/srv/mailboxes/' . $input->db . '/==DOSSIERS==/' . $dossier->nom . '"');
	$subscriptions = file_get_contents('/srv/mailboxes/' . $input->db . '/subscriptions');
	$subscriptions = str_replace('==DOSSIERS==/' . mb_convert_encoding($dossier->nom, "UTF7-IMAP","UTF-8") . "\n", '', $subscriptions);
	@file_put_contents('/srv/mailboxes/' . $input->db . '/subscriptions', $subscriptions);
	return array("code" => 200);
}
?>
