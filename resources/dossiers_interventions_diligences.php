<?php
include_once 'api_optimus/functions.php';

function read($connection,$input)
{
	$input->table = 'dossiers_interventions_diligences';
	
	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, $input->id);
	if ($authorizations['read'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour accéder à ce dossier");
	
	if (isset($input->id) && isset($input->subid) && isset($input->subsubid))
		$diligences = $connection->query("SELECT * FROM `" . $input->db . "`." . $input->table . " WHERE intervention = " . $input->subid . " AND id = " . $input->subsubid)->fetchAll(PDO::FETCH_ASSOC);
	else
		$diligences = $connection->query("SELECT * FROM `" . $input->db . "`." . $input->table . " WHERE intervention = " . $input->subid . " ORDER BY date ASC")->fetchAll(PDO::FETCH_ASSOC);
	
	return array("code" => 200, "data" => $diligences, "authorizations" => $authorizations);
}


function create($connection,$input)
{
	$input->table = 'dossiers_interventions_diligences';

	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, $input->id);
	if ($authorizations['write'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier ce dossier");
	
	$input->fields = validate_fields($connection, $input->table, $input->body);
	
	$query = "INSERT INTO `" . $input->db . "`." . $input->table . " SET ";
	foreach($input->body as $key => $value)
		if ($key!='id')
			$query .= $key.'=:'.$key.',';
	$query = substr($query,0,-1);
	
	$diligence = $connection->prepare($query);
	foreach($input->body as $key => $value)
		if ($key!='id')
			bind_param($diligence, $key, $value, $input->fields[$key]);
	
	if($diligence->execute())
	{
		$new_id = $connection->lastInsertId();
		$new_diligence = $connection->query("SELECT * FROM `" . $input->db . "`.dossiers_interventions_diligences WHERE id = " . $new_id)->fetch(PDO::FETCH_ASSOC);
		return array("code" => 201, "data" => $new_diligence, "authorizations" => $authorizations);
	}
	else
		return array("code" => 400, "message" => $diligence->errorInfo()[2]);
}


function modify($connection,$input)
{
	$input->table = 'dossiers_interventions_diligences';
	if (!isset($input->subid)) return array("code" => 400, "message" => "Aucun identifiant de fiche d'intervention n'a été renseigné");
	if (!isset($input->subsubid)) return array("code" => 400, "message" => "Aucun identifiant de diligence n'a été renseigné");
	if (!isset($input->body)) return array("code" => 400, "message" => "Aucune donnée à modifier n'a été transmise");
	if (isset($input->body->date) && !preg_match("/\d{4}\-\d{2}-\d{2}/", $input->body->date))
		return array("code" => 400, "message" => "Date invalide");
	if (isset($input->body->commission) && ($input->body->commission < 0 OR $input->body->commission > 0.99))
		return array("code" => 400, "message" => "Le champ 'commission' doit être compris entre 0 et 0.99");
	
	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, @$input->id);
	if ($authorizations['write'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour effectuer cette action");
	
	$input->fields = validate_fields($connection, $input->table, $input->body);
	
	$exists = $connection->query("SELECT id FROM `" . $input->db . "`." . $input->table . " WHERE id = " . $input->subsubid);
	if ($exists->rowCount() == 0)
		return array("code" => 404, "message" => "Cette diligence n'existe pas");
	
	$query = "UPDATE `" . $input->db . "`." . $input->table . " SET ";
	foreach($input->body as $key => $value)
		if ($key!='id')
			$query .= $key.'=:'.$key.',';
	$query = substr($query,0,-1);
	$query .= " WHERE id = '" . $input->subsubid . "'";

	$diligence = $connection->prepare($query);
	foreach($input->body as $key => $value)
		if ($key!='id')
			bind_param($diligence, $key, $value, $input->fields[$key]);
	
	if($diligence->execute())
		return array("code" => 200, "authorizations" => $authorizations);
	else
		return array("code" => 400, "message" => $diligence->errorInfo()[2]);
}


function delete($connection,$input)
{
	$input->table = 'dossiers_interventions_diligences';
	if (!isset($input->subid)) return array("code" => 400, "message" => "Aucun identifiant de fiche d'intervention n'a été renseigné");
	if (!isset($input->subsubid)) return array("code" => 400, "message" => "Aucun identifiant de diligence n'a été renseigné");
	
	$diligence_exists = $connection->query("SELECT * FROM `" . $input->db . "`.dossiers_interventions_diligences WHERE id = " . $input->subsubid, PDO::FETCH_OBJ);
	if ($diligence_exists->rowCount() == 0)
		return array("code" => 404, "message" => "Cette diligence n'existe pas");
	$diligence_exists = $diligence_exists->fetchObject();
	
	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, $input->id);
	if ($authorizations['write'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier ce dossier");
	
	if (!$connection->query("DELETE FROM `" . $input->db . "`.dossiers_interventions_diligences WHERE id = " . $input->subsubid))
		return array("code" => 400, "message" => $connection->errorInfo()[2]);
	
	return array("code" => 200);
}
?>
