<?php
function read($connection,$input)
{
	if (!preg_match('/^[a-z0-9_]+$/', @$input->body->module)) return array("code" => 400, "message" => "Nom de module absent ou invalide");
	
	if (@$input->body->settings AND !is_array($input->body->settings))
		$input->body->settings = array($input->body->settings);
	
	$query = "SELECT * FROM `" . $input->db . "`.settings WHERE user = :user AND module = :module AND (";
	if (@$input->body->settings)
		foreach ($input->body->settings as $setting)
			$query .= 'setting = :' . $setting . ' OR  '; 
	$query = substr($query ,0,-5) . (@$input->body->settings?')':'');

	$settings = $connection->prepare($query);
	$settings->bindValue(':user',  $input->body->user, PDO::PARAM_STR);
	$settings->bindValue(':module',  $input->body->module, PDO::PARAM_STR);
	if (@$input->body->settings)
		foreach ($input->body->settings as $setting)
			$settings->bindValue(':'.$setting, $setting, PDO::PARAM_STR);
	
	if (!$settings->execute())
		return array("code" => 400, "message" => $settings->errorInfo()[2]);
	
	$results = array();
	while ($setting = $settings->fetch(PDO::FETCH_ASSOC))
		$results[$setting['setting']] = $setting['value'];
	return array("code" => 200, "data" => @$results);
}


function create($connection,$input)
{
	if (!preg_match('/^[a-z0-9_]+$/', @$input->body->module)) return array("code" => 400, "message" => "Nom de module absent ou invalide");
	if (@!$input->body->settings OR !is_object($input->body->settings)) return array("code" => 400, "message" => "La variable 'settings' est absente ou mal formatée");
	
	foreach($input->body->settings as $key => $value)
	{
		if (!preg_match('/^[a-z0-9_]+$/', $key))
			return array("code" => 400, "message" => "Le réglage " . $key . " n'est pas au bon format");
		$settings = $connection->prepare("REPLACE INTO `" . $input->db . "`.settings VALUES(:user,:module,:setting,:value)");
		$settings->bindValue(':user', $input->body->user, PDO::PARAM_STR);
		$settings->bindValue(':module', $input->body->module, PDO::PARAM_STR);
		$settings->bindValue(':setting', $key, PDO::PARAM_STR);
		$settings->bindValue(':value', json_encode($value), PDO::PARAM_STR);
		if (!$settings->execute())
			return array("code" => 400, "message" => $settings->errorInfo()[2]);
	}
	return array("code" => 200);
}

function modify($connection,$input)
{
	return array("code" => 501, "message" => 'Méthode non implémentée');
}

function delete($connection,$input)
{
	if (!preg_match('/^[a-z0-9_]+$/', @$input->body->module)) return array("code" => 400, "message" => "Nom de module absent ou invalide");
	
	if (@$input->body->settings AND !is_array($input->body->settings))
		$input->body->settings = array($input->body->settings);
	
	$query = "DELETE FROM `" . $input->db . "`.settings WHERE user = :user AND module = :module AND (";
	if (@$input->body->settings)
		foreach ($input->body->settings as $setting)
			$query .= 'setting = :' . $setting . ' OR  '; 
	$query = substr($query ,0,-5) . (@$input->body->settings?')':'');
	
	$settings = $connection->prepare($query);
	$settings->bindValue(':user',  $input->user, PDO::PARAM_STR);
	$settings->bindValue(':module',  $input->body->module, PDO::PARAM_STR);
	if (@$input->body->settings)
		foreach ($input->body->settings as $setting)
			$settings->bindValue(':'.$setting, $setting, PDO::PARAM_STR);
	
	if ($settings->execute())
		return array("code" => 200, "data" => @$results);
	else
		return array("code" => 400, "message" => $settings->errorInfo()[2]);
}
?>
