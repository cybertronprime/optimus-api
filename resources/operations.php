<?php
include_once 'api_optimus/functions.php';

function read($connection,$input)
{
	$input->table = 'compta_operations';
	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, @$input->id);
	if ($authorizations['read'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour accéder à cette opération");
	
	if (@$input->id)
	{
		if (@$input->body)
		{
			$input->fields = validate_fields($connection, $input->table, array_combine($input->body,$input->body));
			$operation = $connection->prepare("SELECT " . implode(',', $input->body) . " FROM `" . $input->db . "`." . $input->table . " WHERE id = :id");
		}
		else
			$operation = $connection->prepare("SELECT * FROM `" . $input->db . "`." . $input->table . " WHERE id = :id");
		$operation->bindParam(':id', $input->id, PDO::PARAM_INT);
		$operation->execute();
		if ($operation->rowCount() == 0)
			return array("code" => 404, "message" => "Cette facture n'existe pas");
		else
		{
			$operation = $operation->fetchAll(PDO::FETCH_ASSOC);
			return array("code" => 200, "data" => $operation, "authorizations" => $authorizations);
		}
	}
	else if (@$input->body)
	{
		if (isset($input->body->fields))
		{
			$input->fields = validate_fields($connection, $input->table, array_combine($input->body->fields,$input->body->fields));
			$query = "SELECT " . implode(',', $input->body->fields) . " FROM `" . $input->db . "`." . $input->table . ' WHERE ';
		}
		else
			$query = "SELECT * FROM `" . $input->db . "`." . $input->table . ' WHERE ';
		
		if (@$input->body->filters)
			foreach(@$input->body->filters as $filter)
				foreach($filter as $key => $value)
					$query .= $key.'=:'.$key.'  AND  ';
		$query = substr($query,0,-7);

		$operations = $connection->prepare($query);
		if (@$input->body->filters)
			foreach(@$input->body->filters as $filter)
				foreach($filter as $key => $value)
					bind_param($operations, $key, $value, @$input->fields[$key]);
		
		if($operations->execute())
			return array("code" => 200, "data" => $operations->fetchAll(PDO::FETCH_ASSOC), "authorizations" => $authorizations);
		else
			return array("code" => 400, "message" => $operations->errorInfo()[2]);
	}
	else
	{
		$operations =  $connection->query("SELECT * FROM `" . $input->db . "`." . $input->table);
		return array("code" => 200, "data" => $operations->fetchAll(PDO::FETCH_ASSOC), "authorizations" => $authorizations);
	}
	return array("code" => 400, "message" => "Il n'a été renseigné ni 'identifiant' ni 'champs' dans la requête");
}

function create($connection,$input)
{
	return array("code" => 501, "message" => 'Méthode non implémentée');
}

function modify($connection,$input)
{
	return array("code" => 501, "message" => 'Méthode non implémentée');
}

function delete($connection,$input)
{
	return array("code" => 501, "message" => 'Méthode non implémentée');
}
?>
