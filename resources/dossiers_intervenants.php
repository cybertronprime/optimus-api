<?php
include_once 'api_optimus/functions.php';

function read($connection,$input)
{
	$input->table = 'dossiers_intervenants';
	
	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, $input->id);
	if ($authorizations['read'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour accéder à ce dossier");
	
	$dossier = $connection->query("SELECT * FROM `" . $input->db . "`.dossiers WHERE id = " . $input->id);
	if ($dossier->rowCount() == 0)
		return array("code" => 404, "message" => "Ce dossier n'existe pas");
	
	$intervenants = $connection->query("SELECT * FROM `" . $input->db . "`." . $input->table . " WHERE dossier = " . $input->id);
	$intervenants = $intervenants->fetchAll(PDO::FETCH_ASSOC);
	return array("code" => 200, "data" => $intervenants, "authorizations" => $authorizations);
}


function create($connection,$input)
{
	if (!preg_match("/^\d+$/", $input->body->contact)) return array("code" => 400, "message" => "Identifiant intervenant invalide");
	if ($input->body->contact == 0) return array("code" => 400, "message" => "Un identifiant d'intervenant doit être renseigné");
	if (!preg_match("/^\d+$/", $input->body->qualite)) return array("code" => 400, "message" => "Identifiant qualite invalide");
	if ($input->body->qualite == 0) return array("code" => 400, "message" => "Un identifiant qualite doit être renseigné");
	if (@$input->body->lien && !preg_match("/^\d+$/", $input->body->lien)) return array("code" => 400, "message" => "Identifiant lien invalide");

	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, $input->id);
	if ($authorizations['write'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier à ce dossier");
	
	$intervenant_exists = $connection->prepare("SELECT * FROM `" . $input->db . "`.dossiers_intervenants WHERE dossier = :dossier AND contact = :contact AND qualite = :qualite AND lien = :lien");
	$intervenant_exists->bindParam(':dossier', $input->id, PDO::PARAM_INT);
	$intervenant_exists->bindParam(':contact', $input->body->contact, PDO::PARAM_INT);
	$intervenant_exists->bindParam(':qualite', $input->body->qualite, PDO::PARAM_INT);
	$intervenant_exists->bindParam(':lien', $input->body->lien, PDO::PARAM_INT);
	$intervenant_exists->execute();
	if ($intervenant_exists->rowCount() > 0)
		return array("code" => 400, "message" => "Cet intervenant existe déjà");
	
	if (@!$input->body->lien)
		$input->body->lien = 0;
	
	$intervenant = $connection->prepare("INSERT INTO `" . $input->db . "`.dossiers_intervenants SET dossier = :dossier, contact = :contact, qualite = :qualite, lien = :lien");
	$intervenant->bindParam(':dossier', $input->id, PDO::PARAM_INT);
	$intervenant->bindParam(':contact', $input->body->contact, PDO::PARAM_INT);
	$intervenant->bindParam(':qualite', $input->body->qualite, PDO::PARAM_INT);
	$intervenant->bindParam(':lien', $input->body->lien, PDO::PARAM_INT);
	if($intervenant->execute())
		return array("code" => 201, "data" => $connection->lastInsertId(), "authorizations" => $authorizations);
	else
		return array("code" => 400, "message" => $intervenant->errorInfo()[2]);
}


function modify($connection,$input)
{
	return array("code" => 501, "message" => 'Méthode non implémentée');
}


function delete($connection,$input)
{
	if (!isset($input->subid))
		return array("code" => 400, "message" => "Aucun identifiant n'a été renseigné");
	
	$intervenant_exists = $connection->query("SELECT * FROM `" . $input->db . "`.dossiers_intervenants WHERE id = " . $input->subid, PDO::FETCH_OBJ);
	if ($intervenant_exists->rowCount() == 0)
		return array("code" => 404, "message" => "Cet intervenant n'existe pas");
	$intervenant_exists = $intervenant_exists->fetchObject();
	
	$authorizations_dossier = get_authorizations($connection, $input->db, $input->user, $input->resource, $input->id);
	if ($authorizations_dossier['write'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier ce dossier");
	
	if (!$connection->query("DELETE FROM `" . $input->db . "`.dossiers_intervenants WHERE id = " . $input->subid))
		return array("code" => 400, "message" => $connection->errorInfo()[2]);
	
	if (!$connection->query("DELETE FROM `" . $input->db . "`.dossiers_intervenants WHERE dossier = " . $input->id . " AND lien= " . $intervenant_exists->contact))
		return array("code" => 400, "message" => $connection->errorInfo()[2]);
	
	return array("code" => 200);
}
?>
