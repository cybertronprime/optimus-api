<?php
$version_date = trim(file('/srv/api/api_optimus/VERSION')[0]);

$config = file_get_contents("/srv/api/api_optimus/config.php");
exec('rm -R /srv/api/api_optimus');
exec('mkdir /srv/api/api_optimus');
exec('git clone https://gitlab.com/cybertronprime/optimus-api.git /srv/api/api_optimus/.');
file_put_contents("/srv/api/api_optimus/config.php",$config);

$errors = array();
$user_sql_files = array_diff(scandir('/srv/api/api_optimus/sql/user'), array('..', '.'));
$users_query = $db->query("SELECT TABLE_SCHEMA as db FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'dossiers_intervenants'");
while($user = $users_query->fetch(PDO::FETCH_ASSOC))
	foreach($user_sql_files as $user_sql_file)
		if (preg_replace("/[^0-9]/","",$user_sql_file) > $version_date)
			exec("mariadb -f -u" . $username . " -p" . $password . " '" . $user['db'] . "' < '/srv/api/api_optimus/sql/user/" . $user_sql_file . "' 2>&1", $errors);

$errors2 = array();
$structure_sql_files = array_diff(scandir('/srv/api/api_optimus/sql/structure'), array('..', '.'));
$structures_query = $db->query("SELECT TABLE_SCHEMA as db FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'factures_paiements'");
while($structure = $structures_query->fetch(PDO::FETCH_ASSOC))
	foreach($structure_sql_files as $structure_sql_file)
		if (preg_replace("/[^0-9]/","",$structure_sql_file) > $version_date)
			exec("mariadb -f -u" . $username . " -p" . $password . " '" . $structure['db'] . "' < '/srv/api/api_optimus/sql/structure/" . $structure_sql_file . "' 2>&1", $errors2);

$errors = array_merge(@$errors, @$errors2);

if (sizeof(@$errors)>0)
	die(json_encode(array("code" => 400, "message" => implode("\n",$errors))));
else
	die(json_encode(array("code" => 200, "message" => 'Mise à jour effectuée avec succès')));
?>