<?php
include_once 'api_optimus/functions.php';

function read($connection,$input)
{
	include_once 'api_optimus/datagrid.php';
	$input->table = 'factures';
	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, @$input->id);
	if ($authorizations['read'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour accéder à cette facture");
	
	if (@$input->body->format == 'pdf' AND @$input->id)
	{
		include_once 'api_optimus/facture_generator/pdf.php';
		exit;
	}
	else if (@$input->id)
	{
		if (@$input->body)
		{
			$input->fields = validate_fields($connection, $input->table, array_combine($input->body,$input->body));
			$facture = $connection->prepare("SELECT " . implode(',', $input->body) . " FROM `" . $input->db . "`." . $input->table . " WHERE id = :id");
		}
		else
			$facture = $connection->prepare("SELECT * FROM `" . $input->db . "`." . $input->table . " WHERE id = :id");
		$facture->bindParam(':id', $input->id, PDO::PARAM_INT);
		$facture->execute();
		if ($facture->rowCount() == 0)
			return array("code" => 404, "message" => "Cette facture n'existe pas");
		else
		{
			$facture = $facture->fetchAll(PDO::FETCH_ASSOC);
			return array("code" => 200, "data" => $facture, "authorizations" => $authorizations);
		}
	}
	else if (@$input->body->fields[0]->name)
	{
		$factures_query = $connection->query("SELECT * FROM `" . $input->db . "`." . $input->table);
		while($facture = $factures_query->fetch(PDO::FETCH_ASSOC))
		{
			if ($facture['server'] == substr($_SERVER['HTTP_HOST'],4))
			{
				$client = $connection->query("SELECT id, COALESCE(NULLIF(TRIM(CONCAT(UPPER(company_name),' ',UPPER(lastname),' ',firstname)),''),'????????') as value FROM `" . $facture['db'] . "`.contacts WHERE id = " . $facture['client'])->fetch(PDO::FETCH_ASSOC);
				$input->dblink->clients[$client['id']] = $client['value'];
				
				$dossier = $connection->query("SELECT id, nom FROM `" . $facture['db'] . "`.dossiers WHERE id = " . $facture['dossier'])->fetch(PDO::FETCH_ASSOC);
				$input->dblink->dossiers[$dossier['id']] = $dossier['nom'];
			}
			$payes = $connection->query("SELECT SUM(amount) as montant FROM `" . $input->db . "`.factures_paiements WHERE facture = " . $facture['id'])->fetch(PDO::FETCH_ASSOC);
			$input->dblink->payes[$facture['id']] = $payes['montant'];
			$input->dblink->restes[$facture['id']] = $facture['total'] - $payes['montant'];
			$input->dblink->ratios[$facture['id']] = ($facture['total']==0?0:round($payes['montant'] / $facture['total'] * 100,2));
		}

		$results = datagrid_request($connection, $input);
		$total = $connection->query('SELECT FOUND_ROWS()')->fetchColumn();
		return array("code" => 200, "data" => $results, 'authorizations' => $authorizations, "total" => $total);
	}
	else if (@$input->body)
	{
		if (isset($input->body->fields))
		{
			$input->fields = validate_fields($connection, $input->table, array_combine($input->body->fields,$input->body->fields));
			$query = "SELECT " . implode(',', $input->body->fields) . " FROM `" . $input->db . "`." . $input->table . ' WHERE ';
		}
		else
			$query = "SELECT * FROM `" . $input->db . "`." . $input->table . ' WHERE ';
		
		if (@$input->body->filters)
			foreach(@$input->body->filters as $filter)
				foreach($filter as $key => $value)
					$query .= $key.'=:'.$key.'  AND  ';
		$query = substr($query,0,-7);

		$factures = $connection->prepare($query);
		if (@$input->body->filters)
			foreach(@$input->body->filters as $filter)
				foreach($filter as $key => $value)
					bind_param($factures, $key, $value, @$input->fields[$key]);
		
		if($factures->execute())
		{
			$data = $factures->fetchAll(PDO::FETCH_ASSOC);
			$total = sizeof($data);
			if (is_array($data) AND $input->body->page AND $input->body->results)
				$data = array_slice($data,($input->body->page-1)*$input->body->results,$input->body->results);
			return array("code" => 200, "data" => $data, "authorizations" => $authorizations, "total" => $total);
		}
		else
			return array("code" => 400, "message" => $factures->errorInfo()[2]);
	}
	else
	{
		$factures =  $connection->query("SELECT * FROM `" . $input->db . "`." . $input->table);
		return array("code" => 200, "data" => $factures->fetchAll(PDO::FETCH_ASSOC), "authorizations" => $authorizations);
	}
	return array("code" => 400, "message" => "Il n'a été renseigné ni 'identifiant' ni 'champs' dans la requête");
}

function create($connection,$input)
{
	$input->table = 'factures';
	
	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, @$input->id);
	if ($authorizations['create'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour effectuer cette action");

	if (@!$input->body->numero)
	{
		$last_numero = $connection->query("SELECT numero FROM `" . $input->db . "`." . $input->table . " WHERE numero LIKE '" . date('y') . "-%' ORDER BY id DESC LIMIT 1")->fetchObject();
		$input->body->numero = date('y') . '-' . str_pad(intval(substr($last_numero->numero,3))+1, 5, "0", STR_PAD_LEFT);
	}
	if (@!$input->body->date)
		$input->body->date = date('Y-m-d');
	
	if (@$input->body)
		$input->fields = validate_fields($connection, $input->table, $input->body);
	
	//$exists = $connection->query("SELECT nom FROM `" . $input->db . "`." . $input->table . " WHERE numero = '" . $input->body->numero . "'");
	//if ($exists->rowCount() > 0)
		//return array("code" => 409, "message" => "Une facture portant ce numéro existe déjà dans la base");
	
	$query = "INSERT INTO `" . $input->db . "`." . $input->table . " SET ";
	
	if (@$input->body)
	{
		foreach($input->body as $key => $value)
			$query .= $key.'=:'.$key.',';
		$query = substr($query,0,-1);

		$facture = $connection->prepare($query);
		foreach($input->body as $key => $value)
			bind_param($facture, $key, $value, $input->fields[$key]);
	}

	if($facture->execute())
	{
		$new_id = $connection->lastInsertId();
		$new_facture = $connection->query("SELECT * FROM `" . $input->db . "`." . $input->table . " WHERE id = " . $new_id)->fetch(PDO::FETCH_ASSOC);
		return array("code" => 201, "data" => $new_facture, "authorizations" => $authorizations);
	}
	else
		return array("code" => 400, "message" => $facture->errorInfo()[2]);
}

function modify($connection,$input)
{
	$input->table = 'factures';
	if (@!$input->body) return array("code" => 400, "message" => "Aucune donnée à modifier n'a été transmise");
	
	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, @$input->id);
	if ($authorizations['write'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour effectuer cette action");
	
	$input->fields = validate_fields($connection, $input->table, $input->body);
	
	$exists = $connection->query("SELECT id FROM `" . $input->db . "`." . $input->table . " WHERE id = " . $input->id);
	if ($exists->rowCount() == 0)
		return array("code" => 404, "message" => "Ce dossier n'existe pas");
	
	$recettes_exists = $connection->query("SELECT id FROM `" . $input->db . "`.compta_recettes WHERE facture = " . $input->id);
	if ($recettes_exists->rowCount() > 0)
		if(isset($input->body->client) OR isset($input->body->dossier) OR isset($input->body->intervention) OR isset($input->body->date) OR isset($input->body->tva))
			return array("code" => 403, "message" => "Cette facture ne peut pas être modifiée car des paiements ont déjà été comptabilisés");
	
	if (isset($input->body->numero))
		return array("code" => 403, "message" => "Le numéro de facture ne peut pas être modifié");
	
	if (isset($input->body->db))
		return array("code" => 403, "message" => "L'avocat ne peut pas être modifié");
	
	
	
	$query = "UPDATE `" . $input->db . "`." . $input->table . " SET ";
	foreach($input->body as $key => $value)
		if ($key!='id')
			$query .= $key.'=:'.$key.',';
	$query = substr($query,0,-1);
	$query .= " WHERE id = '" . $input->id . "'";

	$facture = $connection->prepare($query);
	foreach($input->body as $key => $value)
		if ($key!='id')
			bind_param($facture, $key, $value, $input->fields[$key]);
	
	if($facture->execute())
		return array("code" => 200, "authorizations" => $authorizations);
	else
		return array("code" => 400, "message" => $facture->errorInfo()[2]);
}

function delete($connection,$input)
{
	return array("code" => 403, "message" => "conformément aux règles légales, une facture ne peut pas être supprimée.\nElle ne peut qu'être annulée par un avoir.");
}
?>
