<?php
include_once 'api_optimus/functions.php';

function read($connection,$input)
{
	if ($input->body->action == 'convert_to_pdf')
	{
		$path = str_replace('/files/'. $input->db . '/', '', $input->body->file);
		exec("LC_ALL=fr_FR.UTF-8 export HOME=/srv/api/tmp && libreoffice --headless --convert-to pdf '/srv/files/" . $input->db . "/" . $path . "' --outdir '/srv/files/" . $input->db . "/" . substr($path,0,strrpos($path,'/')) . "' 2>&1",$output);
		if (is_file('/srv/files/' . $input->db . '/' . substr($path,0,strrpos($path,'.')) . '.pdf'))
			return array("code" => 200, "message" => "Conversion effectuée avec succès");
		else
			return array("code" => 400, "message" => "La conversion a échoué");
	}
	else if ($input->body->action == 'convert_to_zip')
	{
		if (!isset($input->body->path)) return array("code" => 400, "message" => "Aucun chemin n'a été spécifié");
		$path = '/srv/files/'. $input->db . str_replace('/files/'. $input->db, '', $input->body->path);
		if (strpos($input->body->path,'/..')) return array("code" => 400, "message" => "Le chemin fourni est invalide");
		if (!is_dir($path)) return array("code" => 400, "message" => "Le chemin spécifié n'existe pas");
		
		if (!isset($input->body->items)) return array("code" => 400, "message" => "Aucun fichier ou dossier à compresser n'a été spécifié");
		foreach ($input->body->items as $item)
			if (strpos($input->body->path,'/')) 
				return array("code" => 400, "message" => "Un des fichiers spécifiés contient un chemin invalide");
		foreach ($input->body->items as $item)
			if (!file_exists($path . '/' . $item))
				return array("code" => 400, "message" => "Un des fichiers spécifiés n'existe pas");
		
		$zip_to_create = time() . ".zip";
		
		exec("LC_ALL=fr_FR.UTF-8 export HOME=/srv/api/tmp && cd '" . $path . "'; zip -r '" . $zip_to_create . "' '" . implode("' '",$input->body->items) . "' 2>&1",$output);
		if (is_file('/srv/files/demo@demoptimus.fr/==DOSSIERS==/' . $zip_to_create))
			return array("code" => 200, "message" => "Conversion effectuée avec succès", "data"=>$zip_to_create);
		else
			return array("code" => 400, "message" => "La conversion a échoué");
	}
	return array("code" => 400, "message" => "Aucune action n'a été renseignée");
}

function create($connection,$input)
{
	return array("code" => 501, "message" => 'Méthode non implémentée');
}

function modify($connection,$input)
{
	return array("code" => 501, "message" => 'Méthode non implémentée');
}

function delete($connection,$input)
{
	return array("code" => 501, "message" => 'Méthode non implémentée');
}
?>
