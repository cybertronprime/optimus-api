<?php
include_once 'api_optimus/functions.php';
function read($connection,$input)
{
	if(!preg_match("/^[a-z0-9_]+$/", $input->body->resource)) die(json_encode(array("code" => 400, "message" => "Resource invalide")));
	if(!preg_match("/^\d+$/", $input->body->id)) die(json_encode(array("code" => 400, "message" => "Identifiant invalide")));
	
	$authorization = get_authorizations($connection, $input->db, $input->user, $input->body->resource, @$input->body->id);
		return(array("code" => 200, "data" => $authorization));
}

function create($connection,$input)
{
	return array("code" => 501, "message" => 'Méthode non implémentée');
}

function modify($connection,$input)
{
	return array("code" => 501, "message" => 'Méthode non implémentée');
}

function delete($connection,$input)
{
	return array("code" => 501, "message" => 'Méthode non implémentée');
}
?>
