<?php
include_once 'api_optimus/functions.php';

function read($connection, $input)
{
	include_once 'api_optimus/datagrid.php';
	$input->table = 'contacts';
	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, @$input->id);
	if ($authorizations['read'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour accéder à ce contact");
	
	if (@$input->id)
	{
		if (@$input->body)
		{
			$input->fields = validate_fields($connection, $input->table, array_combine($input->body,$input->body));
			$contact = $connection->prepare("SELECT " . implode(',', $input->body) . " FROM `" . $input->db . "`." . $input->table . " WHERE id = :id");
		}
		else
			$contact = $connection->prepare("SELECT * FROM `" . $input->db . "`." . $input->table . " WHERE id = :id");
		$contact->bindParam(':id', $input->id, PDO::PARAM_INT);
		$contact->execute();
		if ($contact->rowCount() == 0)
			return array("code" => 404, "message" => "Ce contact n'existe pas");
		else
		{
			$contact = $contact->fetchAll(PDO::FETCH_ASSOC);
			return array("code" => 200, "data" => $contact, "authorizations" => $authorizations);
		}
	}
	else if (@$input->body->fields[0]->name)
	{
		$types = file_get_contents('https://api.optimus-avocats.fr/constants/?data={"db":"contacts_types"}');
		$types = json_decode($types, true);
		foreach ($types['data'] as $type)
			$input->dblink->types[$type['id']] = $type['value'];
		
		$titles = file_get_contents('https://api.optimus-avocats.fr/constants/?data={"db":"contacts_titles"}');
		$titles = json_decode($titles, true);
		foreach ($titles['data'] as $title)
			$input->dblink->titles[$title['id']] = $title['value'];
		
		$companytypes = file_get_contents('https://api.optimus-avocats.fr/constants/?data={"db":"company_types_lvl3"}');
		$companytypes = json_decode($companytypes, true);
		foreach ($companytypes['data'] as $companytype)
			$input->dblink->companytypes[$companytype['id']] = $companytype['value'];
		
		$countries = file_get_contents('https://api.optimus-avocats.fr/constants/?data={"db":"pays"}');
		$countries = json_decode($countries, true);
		foreach ($countries['data'] as $country)
			$input->dblink->countries[$country['id']] = $country['value'];
		
		$categories = file_get_contents('https://api.optimus-avocats.fr/constants/?data={"db":"contacts_categories"}');
		$categories = json_decode($categories, true);
		foreach ($categories['data'] as $category)
			$input->dblink->categories[$category['id']] = $category['value'];
		
		$denominations_query = $connection->query("SELECT id, COALESCE(NULLIF(TRIM(CONCAT(UPPER(IFNULL(company_name,'')),' ',UPPER(lastname),' ',firstname)),''),'????????') as value FROM `" . $input->db . "`." . $input->table);
		while($denomination = $denominations_query->fetch(PDO::FETCH_ASSOC))
			$input->dblink->denominations[$denomination['id']] = $denomination['value'];
			
		//$employeur_query = mysqli_query($connection,"SELECT id, UPPER(company_name) as value FROM " . $_POST['db'] . ".contacts WHERE id > 0");
		//while($employeur = mysqli_fetch_array($employeur_query))
			//$employeurs[$employeur['id']] = $employeur['value'];
			
		$results = datagrid_request($connection, $input);
		$total = $connection->query('SELECT FOUND_ROWS()')->fetchColumn();
		return array("code" => 200, "data" => $results, 'authorizations' => $authorizations, "total" => $total);
	}
	else if (@$input->body)
	{
		if (isset($input->body->fields))
		{
			$input->fields = validate_fields($connection, $input->table, array_combine($input->body->fields,$input->body->fields));
			$query = "SELECT " . implode(',', $input->body->fields) . " FROM `" . $input->db . "`." . $input->table . ' WHERE ';
		}
		else
			$query = "SELECT * FROM `" . $input->db . "`." . $input->table . ' WHERE ';
		
		if (@$input->body->filters)
			foreach(@$input->body->filters as $filter)
				foreach($filter as $key => $value)
					$query .= $key.'=:'.$key.'  AND  ';
		$query = substr($query,0,-7);

		$contacts = $connection->prepare($query);
		if (@$input->body->filters)
			foreach(@$input->body->filters as $filter)
				foreach($filter as $key => $value)
					bind_param($contacts, $key, $value, @$input->fields[$key]);
		
		if($contacts->execute())
		{
			if ($input->body->array_type == 'numeric')
				$data = $contacts->fetchAll(PDO::FETCH_NUM);
			else
				$data = $contacts->fetchAll(PDO::FETCH_ASSOC);
			$total = sizeof($data);
			if (is_array($data) AND $input->body->page AND $input->body->results)
				$data = array_slice($data,($input->body->page-1)*$input->body->results,$input->body->results);
			return array("code" => 200, "data" => $data, "authorizations" => $authorizations, "total" => $total);
		}
		else
			return array("code" => 400, "message" => $contacts->errorInfo()[2]);
	}
	else
	{
		$contacts =  $connection->query("SELECT * FROM `" . $input->db . "`." . $input->table);
		return array("code" => 200, "data" => $contacts->fetchAll(PDO::FETCH_ASSOC), "authorizations" => $authorizations);
	}
	return array("code" => 400, "message" => "Il n'a été renseigné ni 'identifiant' ni 'champs' dans la requête");
}


function create($connection, $input)
{
	$input->table = 'contacts';
	
	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, @$input->id);
	if ($authorizations['create'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour effectuer cette action");
	
	if (@!$input->body)
		$input->body = (object) array();
		
	if (!isset($input->body->lastname))
		$input->body->lastname = 'CLIENT ' . time();
	
	if (@$input->body)
		$input->fields = validate_fields($connection, $input->table, $input->body);

	$query = "INSERT INTO `" . $input->db . "`." . $input->table . " SET ";
	
	if (@$input->body)
	{
		foreach($input->body as $key => $value)
			$query .= $key.'=:'.$key.',';
		$query = substr($query,0,-1);

		$contact = $connection->prepare($query);
		foreach($input->body as $key => $value)
			bind_param($contact, $key, $value, $input->fields[$key]);
	}
	
	if($contact->execute())
	{
		$new_id = $connection->lastInsertId();
		$new_contact = $connection->query("SELECT * FROM `" . $input->db . "`." . $input->table . " WHERE id = " . $new_id)->fetch(PDO::FETCH_ASSOC);
		return array("code" => 201, "data" => $new_contact, "authorizations" => $authorizations);
	}
	else
		return array("code" => 400, "message" => $contact->errorInfo()[2]);
}


function modify($connection, $input)
{
	$input->table = 'contacts';
	if (@!$input->body) return array("code" => 400, "message" => "Aucune donnée à modifier n'a été transmise");

	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, @$input->id);
	if ($authorizations['write'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour effectuer cette action");
	
	$input->fields = validate_fields($connection, $input->table, $input->body);
	
	$exists = $connection->query("SELECT id FROM `" . $input->db . "`." . $input->table . " WHERE id = " . $input->id);
	if ($exists->rowCount() == 0)
		return array("code" => 404, "message" => "Ce contact n'existe pas");

	$query = "UPDATE `" . $input->db . "`." . $input->table . " SET ";
	foreach($input->body as $key => $value)
		if ($key!='id')
			$query .= $key.'=:'.$key.',';
	$query = substr($query,0,-1);
	$query .= " WHERE id = '" . $input->id . "'";
	
	$contact = $connection->prepare($query);
	foreach($input->body as $key => $value)
		if ($key!='id')
			bind_param($contact, $key, $value, $input->fields[$key]);

	if($contact->execute())
		return array("code" => 200, "authorizations" => $authorizations);
	else
		return array("code" => 400, "message" => $contact->errorInfo()[2]);
}


function delete($connection, $input)
{
	$input->table = 'contacts';
	
	$authorizations = get_authorizations($connection, $input->db, $input->user, $input->resource, @$input->id);
	if ($authorizations['delete'] == 0)
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour effectuer cette action");

	$exists = $connection->query("SELECT id FROM `" . $input->db . "`." . $input->table . " WHERE id = '" . $input->id . "'");
	if ($exists->rowCount() == 0)
		return array("code" => 404, "message" => "Ce contact n'existe pas");
	else
		$contact = $exists->fetchObject();

	$intervenants_exists = $connection->query("SELECT id FROM `" . $input->db . "`.dossiers_intervenants WHERE contact = '" . $input->id . "'")->rowCount();
	if ($intervenants_exists > 0)
		return array("code" => 400, "message" => "Ce contact ne peut pas être supprimé car il intervient dans un ou plusieurs dossiers");

  //A FAIRE : CHECKER si le contact apparait dans une facture
  //$factures_exists = $this->conn->query("SELECT id FROM optimus_user_1.interventions WHERE client = '" . $input->id . "' AND db IS NOT NULL")->rowCount();
  //if ($factures_exists > 0)
    //return array("code" => 400, "message" => "Ce contact ne peut pas être supprimé car des factures le concernant ont été émises");

	$contact_delete = $connection->query("DELETE FROM `" . $input->db . "`.contacts WHERE id = '" . $input->id . "'");
	return array("code" => 200);
}
?>
